from django.shortcuts import render
import datetime
from django.contrib import auth
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from accounts.forms import UserCreationForm,AuthenticationForm,PasswordChangeForm,SetPasswordForm
from django.contrib.sessions.models import Session
from django.contrib.auth.hashers import check_password
from django.utils.deprecation import RemovedInDjango21Warning
import warnings
from django.contrib.auth.views import LoginView
from django_otp.plugins.otp_totp.models import TOTPDevice
from django.core.mail import send_mail

from django.contrib import messages
from django.contrib.auth import update_session_auth_hash

from accounts.models import User,loginrecord,balance,mailrecord,coin
from trades.models import order
from depo_n_with.models import withdrawals,deposit

#Exception
def is_get_object_err(e):
    errs=(coin.DoesNotExist,balance.DoesNotExist,loginrecord.DoesNotExist,order.DoesNotExist,deposit.DoesNotExist,withdrawals.DoesNotExist,TOTPDevice.DoesNotExist)
    for err in errs:
        if e.__class__== err:
            msg = e.__str__().split(" ")[0] + " 物件錯誤，或該筆資料不存在"
            return MyModelObjectDoesNotExistError(msg)
    return False

class MyModelObjectDoesNotExistError(Exception):
    pass
class MyValidationError(Exception):
    pass
class MyMailSendError(Exception):
    pass
class MyConfirmStepError(Exception):
    pass
class MyAggregateError(Exception):
    pass
class MyUserlevelError(Exception):
    pass
class MyCoinMaintenceError(Exception):
    pass

#encode
import secrets
from Crypto.Hash import SHA256
def Myencode(*args):
    a = SHA256.new()
    for arg in args:
        a.update(bytes(str(arg), encoding = "utf8"))
    return a.hexdigest()
def Myencode_validate(code,*args):
    a = SHA256.new()
    for arg in args:
        a.update(bytes(str(arg), encoding = "utf8"))
    return a.hexdigest()==code


#getIP sendEmail
def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

from django.template.loader import render_to_string

def email_send(request,user_mail,mail_subject,rout):
    try:
        touser=User.objects.get(username=user_mail)
        t=datetime.datetime.now().timestamp()
        ip=get_client_ip(request)
        s = secrets.token_urlsafe()
        mr=mailrecord(memuuid=touser,subject=mail_subject,timestamp=t,ip=ip,status=1)
        mr.save()
        from_email = "BIDACOIN@bidaex.io"
        title= str(mail_subject)+"From "+ip+" "+str(datetime.datetime.now())
        message="test context"
        if mail_subject and message and from_email:
            code=Myencode(str(touser.username), int(t), s)
            msg_plain = render_to_string(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'templates/email.txt'), {'message': mail.message,"rout":rout,"id":str(touser.pk),"code":code})
            msg_html = render_to_string('email.html', {'message': message,"rout":rout,"id":str(touser.pk),"code":code})
            send_mail(title,msg_plain,from_email, [touser.username],html_message=msg_html,)
            mr.url=s
            mr.status=2
            mr.save()
        else:
            raise MyMailSendError("欄位錯誤")
    except Exception as e:
        raise e

def re_send(request):
    return


def check_userlevel(user):
    if user.level.level>0:
        return True
    return False

def emailVerify(request):
    if request.method=="GET":
        try:
            user_id = request.GET.get('id')
            emailVerifycode = request.GET.get('emailVerifycode')
            user=User.objects.get(id=int(user_id))
            if check_userlevel(user)==False:
                subject="verify email"
                mr=mailrecord.objects.filter(memuuid=user,subject=subject,status=2)
                tmr=mr.order_by('-timestamp')[0]
                if not Myencode_validate(emailVerifycode,user.username,tmr.timestamp,tmr.url): #編碼username,timestamp,url
                    raise MyUserlevelError("email 驗證失敗")
                tmr.status=3
                tmr.save()
                change_level_to='1'
                confirm_level_update(User,int(user_id),change_level_to)
                for one_coin in coin.objects.all():
                    if not balance.objects.filter(memuuid=user, coin=one_coin):
                        b = balance(memuuid=user, coin=one_coin, total=0, canuse=0, freeze=0, btc=0)
                        b.save()
        except Exception as e:
            raise e
            #需要一個成功頁面
        return render(request, "login.html",)



#check level status step


def check_level_ordinary(level,data):
    try:
        if int(data.level.level)+1==int(level):
            return True
        raise MyConfirmStepError("now,user's level is: "+str(data.level)+",next level can't be: "+str(level))
    except Exception as e:
        raise e
def confirm_level_update(MyModel,id,chage_to):
    try:
        w=MyModel.objects.get(id=id)
        check_level_ordinary(chage_to,w)
        MyModel.objects.filter(id=id).update(level=chage_to)
        return True
    except Exception as e:
        if is_get_object_err(e):
            e=is_get_object_err(e)
            raise e
        raise e


#get data
def get_loginrecord_data(request):
    loginrecord_data=list()
    for i in range(0,len(loginrecord.objects.filter(memuuid=request.user.id))):
        if i>5:
            break
        cust_user_data=loginrecord.objects.filter(memuuid=request.user.id).order_by('-timestamp')[i]
        d=datetime.datetime.fromtimestamp(cust_user_data.timestamp).strftime('%Y-%m-%d %H:%M:%S')
        loginrecord_data.append('時間:'+str(d)+'   IP:'+cust_user_data.ip)
    return loginrecord_data

#googleVerify_required
def googleVerify_required(function):
    def googleVerify(request, *args, **kw):
        try:
            if request.user.use_2FA == True:
                if request.user.pass_2FA==False:
                    uid=request.user.id
                    totpd= TOTPDevice.objects.get(user_id=uid)
                    if request.method == 'POST':
                        if totpd.verify_token(request.POST.get('facode',False)):
                            User.objects.filter(pk=uid).update(pass_2FA=True)
                            return function(request, *args, **kw)
                        note=request.POST.get('facode', False) + 'is incorrect!!'
                        return render(request,'test_setgoogle2fa.html',locals())
                    return render(request,'test_setgoogle2fa.html',locals())
                else:
                    return function(request, *args, **kw)
            else:
                return function(request, *args, **kw)
        except Exception as e:
            raise e
    return googleVerify

#function

User = get_user_model()
def register(request):
    if request.method == 'POST':
        try:
            form = UserCreationForm(request.POST)
            if form.is_valid():
                user = form.save()
                user.username=request.POST.get('email')
                user.email=request.POST.get('email')
                user.ip=get_client_ip(request)
                user.save()
                subject = 'Verify'
                email_send(request,user_mail=user.username,mail_subject_is=subject,rout="emailVerify")
                return HttpResponseRedirect('/accounts/login/')
        except Exception as e :
            return HttpResponse(e)
    else:
        #Not post
        form = UserCreationForm()
    return render(request,'register.html',locals())


from django.http import JsonResponse

def login(request, template_name='login.html',redirect_field_name='next',
          authentication_form=AuthenticationForm,extra_context=None, redirect_authenticated_user=False):
    warnings.warn('The login() view is superseded by the class-based LoginView().',RemovedInDjango21Warning, stacklevel=2)
    if request.POST:
        try:
            c=AuthenticationForm(request,request.POST)
            d=c.is_valid()
            print(d)
            msg = {
                'msg': "loginok",'a':str(c.as_p())
            }
            return JsonResponse(msg)
        except Exception as e:
            raise e
    a=request.POST.get("cate")
    if a:
        return render(request, 'login.html', {'data':a})
    if request.method == 'POST':

        # username = request.POST.get('username', None)
        # password = request.POST.get('password', None)
        data= request.POST.get("cate")
        return render(request, 'login.html', {'data':data})

        username = request.POST['mail']
        password = request.POST['pwd']
        try:
            user = User.objects.get(username=username)
            if user.is_login == True:
                msg='同時只能登陸一臺裝置!'
                s=Session.objects.all()
                for ss in s:
                    if str(user.id)==str(ss.get_decoded()['_auth_user_id']):
                        ss.delete()
                        User.objects.filter(username=username).update(is_login=False)
                        User.objects.filter(username=username).update(pass_2FA=False)
                        user = User.objects.get(username=username)
                if user.is_login:
                    return render(request, 'login.html', {'form':authentication_form,'msg': msg})
            if user.login_sta == True:
                return render(request, 'login.html', {'form': authentication_form,'msg': '帳號已經凍結!'})
            if check_password(password, user.password):
                user.is_login = True
                loginrecord_create(user, ip=get_client_ip(request))
                user.save()
                return LoginView.as_view(template_name=template_name, redirect_field_name=redirect_field_name,
                                         form_class=authentication_form, extra_context=extra_context,
                                         redirect_authenticated_user=redirect_authenticated_user)(request)
            return LoginView.as_view(template_name=template_name, redirect_field_name=redirect_field_name,
                                     form_class=authentication_form, extra_context={'msg':msg},
                                     redirect_authenticated_user=redirect_authenticated_user)(request)
        except Exception as e:
            return render(request, 'login.html',{'form':authentication_form})
    return LoginView.as_view(template_name=template_name, redirect_field_name=redirect_field_name,
                             form_class=authentication_form, extra_context=extra_context,
                             redirect_authenticated_user=redirect_authenticated_user)(request)

@login_required
@googleVerify_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return render(request,"user_index.html")
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'chpwd.html', {'form': form})




@login_required
@googleVerify_required
def user_index(request):
    return render(request,"user_index.html")

def index(request):
    return render(request,"index.html")


import geoip2.database
import os
def loginrecord_create(user,ip):
    try:
        gi = geoip2.database.Reader(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+'/GeoLite2-City.mmdb')
        if ip=="127.0.0.1":
            city="test"
        else:
            city = gi.city(ip)
        new_loginrecord=loginrecord(memuuid=user, timestamp=datetime.datetime.now().timestamp(),city=city, ip=ip,status=1.1)
        new_loginrecord.save()
        return
    except Exception as e:
        raise e

def logout(request):
    User.objects.filter(username=request.user.username).update(is_login=False)
    User.objects.filter(username=request.user.username).update(pass_2FA=False)
    auth.logout(request)
    return HttpResponseRedirect('/')



#developing.....
def forgotpassword(request):
    if request.method=="POST":
        try:
            username = request.POST.get('mail')
            sub="Forgot"
            user=User.objects.get(username=username)
            mr=mailrecord.objects.filter(memuuid=user, subject=sub)
            t=datetime.datetime.now().timestamp()
            if mr:
                if int(t)-int(mr.order_by('-timestamp')[0].timestamp)>60:
                    email_send(request,user_mail=user.username,mail_subject_is=sub,rout="resetPassword")
               # email_send(request, user_mail=user.username, mail_subject_is=sub, rout="resetPassword")
            else:
                email_send(request,user_mail=user.username,mail_subject_is=sub,rout="resetPassword")
            return render(request, "forgotpassword.html")#需要一個以寄信頁面
        except Exception as e:
            raise e
    return render(request,"forgotpassword.html")

def resetPassword(request):
    if request.method=="GET":
        try:
            user=User.objects.get(pk=request.GET.get('id'))
            emailVerifycode = request.GET.get('emailVerifycode')
            subject="Forgot"
            mr=mailrecord.objects.filter(memuuid=user,subject=subject,status=2)
            tmr=mr.order_by('-timestamp')[0]
            code=Myencode(user.username,tmr.timestamp,tmr.url)
            if not Myencode_validate(emailVerifycode,user.username,tmr.timestamp,tmr.url):                                                #code尚未設定
                raise MyUserlevelError("email 驗證失敗"+str(user.username)+str(tmr.timestamp)+str(tmr.url)+code)
            tmr.status=3
            tmr.save()
            form = SetPasswordForm(user)
            return render(request, "resetPassword.html", {"form": form})
        except Exception as e:
            raise e
    if request.method == "POST":
        try:
            user=User.objects.get(pk=request.GET.get('id'))
            form=SetPasswordForm(user,request.POST)
            if form.is_valid():
                user = form.save()
                update_session_auth_hash(request, user)  # Important!
                messages.success(request, 'Your password was successfully updated!')
                return render(request,"index.html",)
            else:
                messages.error(request, 'Please correct the error below.')
                return render(request, "resetPassword.html",{"form":form} )
            return render(request, "resetPassword.html", {"form":form})
        except Exception as e:
            #return render(request, "resetPassword.html", {"form":form})
            raise e

'''
def session_test(request):
#    sid = request.COOKIES['sessionid']
    sid='3s3wynnzj3nxe5ln4bcp7bfsx5qoal3h'
#    s_info = 'Session ID:' + sid + '<br>Expire_date:' + str(s.expire_date) + '<br>Data:' + str(s.get_decoded())
    s = Session.objects.all()
    s_info=[]
    for ss in s:
        s_info.append("<li>"+str(ss.get_decoded())+"</li>")
    return HttpResponse(s_info)
'''

@login_required
@googleVerify_required
def open_2FA(request):#need change to show 2FA
    uid=request.user.id
    try:
        totpd= TOTPDevice.objects.get(user_id=uid)
        if User.objects.get(pk=uid).use_2FA:
            if request.method == 'POST':
                if totpd.verify_token(request.POST.get('facode', False)):
                    User.objects.filter(pk=uid).update(pass_2FA=False)
                    User.objects.filter(pk=uid).update(use_2FA=False)
                    totpd.delete()
                    FA_msg="2FA已關閉"
                    return render(request, "inside.html", {'FA_msg':FA_msg})
                note = request.POST.get('facode', False) + '驗證碼錯誤!!請重新輸入'
                return render(request, 'test_setgoogle2fa.html', locals())
            note="輸入2FA以關閉2FA功能"
            return render(request, 'test_setgoogle2fa.html', locals())
    except Exception as e:
        totpd=TOTPDevice.objects.create(user_id=uid)
    qr_url='https://chart.googleapis.com/chart?cht=qr&chs=300x300&chl='+totpd.config_url
    note=str(totpd.key)
    if request.method == 'POST':
        if totpd.verify_token(request.POST.get('facode', False)):
            User.objects.filter(pk=uid).update(pass_2FA=True)
            User.objects.filter(pk=uid).update(use_2FA=True)
            FA_msg="2FA已開啟"
            return render(request, "inside.html", {'FA_msg':FA_msg})
        ans=totpd.verify_token(request.POST.get('facode', False))
        err_msg=request.POST.get('facode', False) +'驗證碼錯誤!!請重新掃描QRcode'
        return render(request, 'test_setgoogle2fa.html', locals())
    return render(request, 'test_setgoogle2fa.html', locals())







