from django.db import models

# Create your models here.
class order(models.Model): #左邊的掛單
    memuuid = models.ForeignKey('accounts.User',on_delete=models.CASCADE)                            #UUID(FK)
    timestamp = models.IntegerField(db_column = 'timestamp' ,null=True)
    oid = models.BigAutoField(db_column='oid',max_length=50,primary_key=True)               #流水號
    account = models.CharField(db_column='account',max_length=50)                           #帳號
    side = models.CharField(db_column='side',max_length=20)                                 #買/賣
    price = models.FloatField(db_column='price',max_length=20,null=True)                              #單價
    qty = models.FloatField(db_column='qty',max_length=20)                                  #數量
    total = models.FloatField(db_column='total',max_length=20)                              #總價
    status = models.CharField(db_column='status',max_length=20,null=True)                   #狀態 0掛單1掛單-交易完成2即時-交易完成-3取消
    cate = models.CharField(db_column='cate',max_length=20)
    symbol = models.ForeignKey('accounts.exchangeinfo',on_delete=models.CASCADE)                               #類型
    tradetime = models.IntegerField(db_column = 'tradetime' ,null=True)
    commission = models.FloatField(db_column='commission',max_length=20, null=True, default = 0)     #BDB
    commission2 = models.FloatField(db_column='commission2',max_length=20, null=True, default = 0)    #others
    relatedid = models.IntegerField(db_column = 'relatedid' ,null=True)
    def order_total(self):
        return self.price*self.qty
class deal(models.Model): #右下的成交
    memuuid = models.ForeignKey('accounts.User',on_delete=models.CASCADE)                            #UUID(FK)
    timestamp = models.BigIntegerField(db_column='timestamp',null=True)                     #timestamp
    oid = models.BigAutoField(db_column='oid',max_length=50,primary_key=True)               #流水號
    side = models.CharField(db_column='side',max_length=20)                                 #買/賣
    price = models.FloatField(db_column='price',max_length=20)                              #單價
    qty = models.FloatField(db_column='qty',max_length=20)                                  #數量
    total = models.FloatField(db_column='total',max_length=20)