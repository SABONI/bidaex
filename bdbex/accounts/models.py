from django.db import models
from django.contrib.auth.models import AbstractUser
import datetime


# Create your models here.

class User(AbstractUser):
    phone = models.CharField(db_column='phone',max_length=20,null=True)                                   #手機
    level = models.ForeignKey('userlevel', default ='0', on_delete=models.CASCADE)                    #會員等級
    statement = models.IntegerField(db_column='statement',null=True,default=0)                         #會員狀態 改整數
    ip = models.CharField(db_column='ip',max_length=20,blank=True)                                     #IP
    language = models.CharField(db_column='language',max_length=20,null=True)                          #語言
    recommend_num = models.CharField(db_column='recommend_num',max_length=20,null=True)              #自己的推薦碼
    recommend_num_from = models.CharField(db_column='recommend_num_from',max_length=20,null=True)          #別人的推薦碼
    use_api = models.BooleanField(default=False)                                                       #API是否使用
    use_2FA = models.BooleanField(default=False)                                                       #2FA是否使用
    pass_2FA=models.BooleanField(default=False)                                                        #2FA是否通過
    use_SMS = models.BooleanField(default=False)                                                       #SMS是否使用
    use_Whitelist= models.BooleanField(default=False)                                                  #Whitelist是否使用
    pass_SMS = models.BooleanField(default=False)                                                      #SMS是否通過
    use_bdbfee = models.BooleanField(default=False)                                                    #bdbfee是否使用
    login_sta = models.BooleanField(default=False)                                                     #登入是否鎖定
    mail_sta = models.BooleanField(default=False)                                                      # 信箱是否鎖定
    trade_sta = models.BooleanField(default=False)                                                     # 交易是否鎖定
    withdraw_sta = models.BooleanField(default=False)                                                  # 提線是否鎖定
    deposite_sta = models.BooleanField(default=False)                                                  # 存款是否鎖定
    is_login = models.BooleanField(default=False)                                                      #帳號是否登入中
    cancel_time = models.IntegerField(db_column = 'cancel_time', default=0)        #取消時間
    #↑這邊要注意的是 這個時間是此週期的始時間
    cancel_num = models.IntegerField(db_column = 'cancel_num', default=0)          #期間取消次數

    def time_registered(self):
        dt=self.date_joined.replace(tzinfo=None)
        dt_now=datetime.datetime.now()
        return dt_now-dt
    def __str__(self) :
        return self.username

class realname(models.Model):
    memuuid = models.ForeignKey('User',on_delete=models.CASCADE)                             #UUID(FK)
    area = models.CharField(db_column='area',max_length=20,null=True)                                    #地區
    lastname = models.CharField(db_column='lastname',max_length=20,null=True)                            #姓
    firstname = models.CharField(db_column='firstname',max_length=20)                                     #名
    idnumber = models.ImageField(db_column='idnumber',null=True)                                           #身分證號
    idcardf = models.ImageField(db_column='idcardf',null=True)                                             #身分證正面
    idcardb = models.ImageField(db_column='idcardb',null=True)                                             #身分證反面
    idcardwithhand = models.ImageField(db_column='idcardwithhand',null=True)                               #手持照

class api(models.Model):
    memuuid = models.ForeignKey('User',on_delete=models.CASCADE)                                #UUID(FK)
    note = models.CharField(db_column='note',max_length=20,null=True)                                    #備註
    key = models.CharField(db_column='key',max_length=50,null=True)                                      #KEY
    secretkey = models.CharField(db_column='secretkey',max_length=50,null=True)                          #金鑰


class address(models.Model): #提現地址管理
    memuuid = models.ForeignKey('User',on_delete=models.CASCADE)                             #UUID(FK=>User
    coin = models.ForeignKey('coin',on_delete=models.CASCADE)                                #幣值名(FK=>coin
    note = models.CharField(db_column='note', max_length=50)                                 #地址備註
    address = models.CharField(db_column='address', max_length=50)

class balance(models.Model): #資產管理
    memuuid = models.ForeignKey('User',on_delete=models.CASCADE)                             #UUID(FK=>User
    coin = models.ForeignKey('coin',on_delete=models.CASCADE)                                #幣值名(FK=>coin
    total = models.FloatField(db_column='total', max_length=20)                              #總
    canuse = models.FloatField(db_column='canuse', max_length=20)                            #可使用
    freeze = models.FloatField(db_column='freeze', max_length=20)                            #下單凍結
    btc = models.FloatField(db_column='btc', max_length=20)

class loginrecord(models.Model): #登入紀錄
    memuuid = models.ForeignKey('User',on_delete=models.CASCADE)                             #UUID(FK)
    timestamp = models.BigIntegerField(db_column='timestamp')                        #timestamp
    ip = models.CharField(db_column='ip', max_length=20)                           #狀態
    city=models.CharField(db_column='city', max_length=20)
    status = models.FloatField(db_column='status', max_length=50)

class mailrecord(models.Model):
    memuuid = models.ForeignKey('User', on_delete=models.CASCADE)
    subject = models.ForeignKey('mails',on_delete=models.CASCADE)
    timestamp = models.BigIntegerField(db_column='timestamp')
    ip = models.CharField(db_column='ip', max_length=20)
    url= models.CharField(db_column='url', max_length=50)
    status = models.IntegerField(default=0) #0fail 1sending 2sent 3expired 4used

class coin(models.Model): #幣資料
    coin = models.CharField(db_column='coin', max_length=20,primary_key=True)                       #幣值 PK
    fullname = models.CharField(db_column='fullname', max_length=20)                                #全名
    minwithdrawal = models.FloatField(db_column='minwithdrawal', max_length=20)                     #最小顆數
    exfee = models.FloatField(db_column='exfee', max_length=20)                                     #手續費
    imgurl = models.CharField(db_column='imgurl', max_length=50)                                    #圖片網址
    status = models.CharField(db_column='status', max_length=20, null=True)                         #貨幣狀態、可否提 1都可 2 3 4都不行
    def __str__(self) :
        return self.coin

class exchangeinfo(models.Model): #交易管理
    symbol = models.CharField(db_column='symbol', max_length=20,primary_key=True)                    #交易sumbol PK
    status = models.CharField(db_column='status', max_length=20,null=True)
    baseasset = models.CharField(db_column='baseasset', max_length=20,null=True)                     #交易貨幣
    baseassetprecision = models.IntegerField(db_column='baseassetprecision', null=True)              #位數限制
    quoteasset = models.CharField(db_column='quoteasset', max_length=20,blank=True)                  #交易主貨幣
    quoteprecision = models.IntegerField(db_column='quoteprecision',null=True)
    ordertypes = models.CharField(db_column='ordertypes', max_length=20,null=True)
    minprice = models.FloatField(db_column='minprice', max_length=20,null=True)                      #價格限制
    maxprice = models.FloatField(db_column='maxprice', max_length=20,null=True)
    minQty = models.FloatField(db_column='minQty', max_length=20,null=True)                          #數量限制
    maxQty = models.FloatField(db_column='maxQty', max_length=20,null=True)
    def __str__(self) :
        return self.symbol

class fee(models.Model): #放手續費的地方
    fid = models.BigAutoField(db_column='fid', max_length=100,primary_key=True)             #流水號
    coin = models.CharField(db_column='coin', max_length=20)                                #幣值名
    name = models.CharField(db_column='name', max_length=20)                                #全名
    total = models.FloatField(db_column='total', max_length=20, default = 0)                             #總

class userlevel(models.Model): #user等級對照
    level = models.IntegerField(db_column='level', primary_key=True)                        #user等級
    discount = models.FloatField(db_column='discount')                                      #no BDB對照的折扣
    bdbdiscount = models.FloatField(db_column='bdbdiscount')                                      #BDB對照的折扣