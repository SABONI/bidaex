from django.shortcuts import render

import time
import sys
from django.db.models import Max, Q, Sum
from django.db import transaction
from django.http import HttpResponse
import json
# def confirm(timestamp, account, side, price, qty, total, status, cate, tradetime, commission, memuuid_id, symbol_id, relatedid, commission2):
#	try to insert order_tmp(not check) to order(checked)

from accounts.models import User,balance,fee,exchangeinfo
from trades.models import order


##########test#############
def getorder(request):
    try:
        def allorder(limit):
            try:
                limit = int(limit)
                Res = {}
                mess = []
                res = order.objects.all().filter(status='2').order_by("timestamp")[:limit]
                for res in res:
                    Res = {"timestamp": res.timestamp, "oid": res.oid, "side": res.side, "symbol": res.symbol_id,
                           "price": res.price, "qty": res.qty, "total": res.total, "BDBcommission": res.commission,
                           "Coincommission": res.commission2}
                    mess.append(Res)
                return json.dumps(mess)
                if mess == []:
                    Res = Response("ok", "no order")
                return HttpResponse(json.dumps(Res), content_type="application/json")
            except:
                tp, val, tb = sys.exc_info()
                error = str(val), str(tb)
                print(str(error), str(tb.tb_lineno))
                Res = Response(-1130, "INVALID_PARAMETER")
                return HttpResponse(json.dumps(Res), content_type="application/json")

        Res = {}
        if request.method == "GET":
            limit = request.GET.get("limit")
            if limit == "" or limit == None:
                limit = 500
                res = allorder(limit)
                return HttpResponse(res)
            elif limit.isdigit():
                res = allorder(limit)
                return HttpResponse(res)
            else:
                Res = Response(-1130, "INVALID_PARAMETER")
                return HttpResponse(json.dumps(Res), content_type="application/json")

        return render(request, "getorder.html", locals())

    except Exception as e:
        tp, val, tb = sys.exc_info()
        error = str(val), str(tb)
        print(str(error), str(tb.tb_lineno))


###########################
def historyorder(request):
    try:
        def hisorder(category, limit):
            limit = int(limit)
            category = int(category)
            try:
                if category == 0:  # all
                    Res = {}
                    mess = []
                    res = order.objects.all().filter(account=account).order_by("-timestamp")[:limit]
                    for res in res:
                        Res = {"oid": res.oid, "side": res.side, "symbol": res.symbol_id, "price": res.price,
                               "qty": res.qty, "total": res.total, "BDBcommission": res.commission,
                               "Coincommission": res.commission2}
                        mess.append(Res)

                    return json.dumps(mess)
                    if mess == []:
                        Res = Response("ok", "no order")
                        return HttpResponse(json.dumps(Res), content_type="application/json")

                    return mess
                elif category == 1:  # finished trade
                    Res = {}
                    mess = []
                    res = order.objects.all().filter(account=account).filter(Q(status='2') | Q(status='1')).order_by(
                        "-timestamp")[:limit]
                    for res in res:
                        Res = {"oid": res.oid, "side": res.side, "symbol": res.symbol_id, "price": res.price,
                               "qty": res.qty, "total": res.total, "BDBcommission": res.commission,
                               "Coincommission": res.commission2}
                        mess.append(Res)
                        return json.dumps(mess)
                    if mess == []:
                        Res = Response("ok", "no order")
                        return HttpResponse(json.dumps(Res), content_type="application/json")

                    return mess
                elif category == 2:  # not finish trade
                    Res = {}
                    mess = []
                    res = order.objects.all().filter(account=account, status=0).order_by("-timestamp")[:limit]
                    for res in res:
                        Res = {"oid": res.oid, "side": res.side, "symbol": res.symbol_id, "price": res.price,
                               "qty": res.qty, "total": res.total, "BDBcommission": res.commission,
                               "Coincommission": res.commission2}
                        mess.append(Res)
                    return json.dumps(mess)
                    if mess == []:
                        Res = Response("ok", "no order")
                        return HttpResponse(json.dumps(Res), content_type="application/json")
                    return mess
                else:
                    Res = Response(-1130, "INVALID_PARAMETER")
                    return HttpResponse(json.dumps(Res), content_type="application/json")
            except Exception as e:
                tp, val, tb = sys.exc_info()
                error = str(val), str(tb)
                print(str(error), str(tb.tb_lineno))

        Res = {}
        if request.method == "POST":
            try:
                account = request.user.email
            except:
                Res = Response("error", "you have to login")
                return HttpResponse(json.dumps(Res), content_type="application/json")
            limit = request.POST["limit"]
            category = request.POST["category"]
            if category != '0' and category != '1' and category != '2':
                Res = Response(-1130, "INVALID_PARAMETER")
                return HttpResponse(json.dumps(Res), content_type="application/json")
            if limit == "" or limit == None:
                limit = 500
                res = hisorder(category, limit)
                return HttpResponse(res)
            elif limit.isdigit():
                res = hisorder(category, limit)
                return HttpResponse(res)
            else:
                Res = Response(-1130, "INVALID_PARAMETER")
                return HttpResponse(json.dumps(Res), content_type="application/json")
        return render(request, "historyorder.html", locals())
    except Exception as e:
        tp, val, tb = sys.exc_info()
        error = str(val), str(tb)
        print(str(error), str(tb.tb_lineno))


def check_usebdb(tmp):  # 會知道有沒有用bdbfee 會在res_bdbinfo用到
    res = User.objects.get(username=tmp)
    Res = res.use_bdbfee

    return Res


def res_bdbinfo(tmp):  # 出來的是手續費抽多少的"比例" (request.user, use_bdbfee), output = bdb%, nobdb%,how many bdb
    # feeper = res_bdbinfo(request.user) #手續費%
    # print(list(feeper)[0], list(feeper)[1])   #[0] = 無BDB ， [1] = 有BDB
    try:
        if not tmp.use_bdbfee:  # 沒開BDB
            Res1 = tmp.level.discount
            Res2 = 0
            Res3 = 0
            return Res1, Res2, Res3
        else:
            try:
                bdb = balance.objects.get(coin_id="BDB", memuuid_id=tmp.id)  # uer BDB balance那列

            except Exception as e:
                if "balance matching query does not exist" in str(e):
                    Res1 = tmp.level.discount
                    Res2 = 0
                    Res3 = 0
                    return Res1, Res2, Res3
                else:
                    tp, val, tb = sys.exc_info()
                    error = str(val), str(tb)
                    print(str(error), str(tb.tb_lineno))

            if bdb.canuse > 0:  # 如果BDB有錢 (這是粗略判斷而已)
                Res1 = tmp.level.discount
                Res2 = tmp.level.bdbdiscount  # res = bdb目前的手續費%數
                Res3 = bdb.canuse
                return Res1, Res2, Res3

            else:  # 根本沒錢 (這是粗略判斷而已)
                Res1 = tmp.level.discount
                Res2 = 0  # res = bdb目前的手續費%數
                Res3 = 0
                return Res1, Res2, Res3

    except Exception as e:
        tp, val, tb = sys.exc_info()
        error = str(val), str(tb)
        print(str(error), str(tb.tb_lineno))


# nCreate your views here.
def createorder(request):
    test = order.objects.all()

    def res_bdbvalue(tmp, side):
        try:
            value = order.objects.filter(symbol_id=tmp).order_by("-timestamp")[0]
            if value != []:
                if side == "buy":
                    return value.price
                elif side == "sell":
                    return value.price
        except Exception as e:

            tp, val, tb = sys.exc_info()
            error = str(val), str(tb)
            print(str(error), str(tb.tb_lineno))
            Res = 'fail'
            return Res

    try:

        if request.method == "POST":
            Res = {}
            # 寫入的東西
            account = request.user.email
            price = request.POST["price"]
            qty = request.POST["qty"]
            side = request.POST["side"]
            symbol = request.POST["symbol"]
            cate = request.POST["cate"]
            memuuid = request.user.id

            userbdb = res_bdbinfo(request.user)
            ybdbrate = list(userbdb)[1]
            nbdbrate = list(userbdb)[0]
            bdbuse = list(userbdb)[2]
            use_bdb = check_usebdb(account)

            # print(list(userbdb)[0], list(userbdb)[1])   #[0] = 無BDB ， [1] = 有BDB

            try:
                if not check_userlevel(request.user):
                    Res = Response("error", "請先通過EMAIL認證")
                    return HttpResponse(json.dumps(Res), content_type="application/json")
            except Exception as e:
                tp, val, tb = sys.exc_info()
                error = str(val), str(tb)
                print(str(error), str(tb.tb_lineno))
                Res = Response(-1000, "UNKNOWN")
                return HttpResponse(json.dumps(Res), content_type="application/json")
            try:
                checksymbol = exchangeinfo.objects.get(symbol=symbol)
            except:
                Res = Response(-1121, "Invalid symbol")
                return HttpResponse(json.dumps(Res), content_type="application/json")
            exinfo = exchangeinfo.objects.get(symbol=symbol)
            statustmp = str(exinfo.ordertypes).split(",")
            if exinfo.status == "1":
                baseprn = int(exinfo.baseassetprecision)
                quoteprn = int(exinfo.quoteprecision)
                maxqty = float(exinfo.maxQty)
                minqty = float(exinfo.minQty)
                maxprice = float(exinfo.maxprice)
                minprice = float(exinfo.minprice)
                if (cate != "limit" and cate != "market"):
                    Res = Response(-1103, "UNKNOWN_PARAM")
                    return HttpResponse(json.dumps(Res), content_type="application/json")
                elif (side != "buy" and side != "sell"):
                    Res = Response(-1117, "Invalid side.")
                    return HttpResponse(json.dumps(Res), content_type="application/json")
                try:
                    float(qty)
                except:
                    Res = Response(-1103, "UNKNOWN_PARAM")
                    return HttpResponse(json.dumps(Res), content_type="application/json")
                if float(qty) <= 0:
                    Res = Response(-901, "Filter failure: LOT_SIZE")
                    return HttpResponse(json.dumps(Res), content_type="application/json")
                if float(qty) < minqty or float(qty) > maxqty:
                    Res = Response(-901, "Filter failure: LOT_SIZE")
                    return HttpResponse(json.dumps(Res), content_type="application/json")

                with transaction.atomic():
                    if side == "buy":
                        add = symbol.split("_")[0]  # left will increase
                        add2 = symbol.split("_")[1]
                    elif side == "sell":
                        add = symbol.split("_")[1]  # right will increase
                        add2 = symbol.split("_")[0]
                    sym = add + "_BDB"
                    sym2 = add2 + "_BDB"
                    value = res_bdbvalue(sym, side)  # 找出買賣的幣和bdb的價值
                    value2 = res_bdbvalue(sym2, side)

                    if value == 'fail':
                        Res = Response("error", "BDB value problem")
                        return HttpResponse(json.dumps(Res), content_type="application/json")
                    if cate == 'limit' and side == 'buy' and ('limit' in statustmp):  # ex: limit buy ADA_USDT
                        try:
                            float(price)
                        except:
                            Res = Response(-1130, "INVALID_PARAMETER")

                            return HttpResponse(json.dumps(Res), content_type="application/json")
                        if float(price) <= 0:
                            Res = Response(-1130, "INVALID_PARAMETER")
                            return HttpResponse(json.dumps(Res), content_type="application/json")
                        if "." in str(price):
                            if len(price.split(".")[1]) > quoteprn:
                                Res = Response(-1130, "INVALID_PARAMETER")
                                return HttpResponse(json.dumps(Res), content_type="application/json")
                        if float(price) > maxprice or float(price) < minprice:
                            Res = Response(-900, "Filter failure: PRICE_FILTER")
                            return HttpResponse(json.dumps(Res), content_type="application/json")
                        Res = limitbuy(account, price, qty, symbol, memuuid, ybdbrate, nbdbrate, bdbuse, value, value2)
                        return HttpResponse(json.dumps(Res), content_type="application/json")
                    elif cate == 'limit' and side == 'sell' and ('limit' in statustmp):
                        try:
                            float(price)
                        except:
                            Res = Response(-1130, "INVALID_PARAMETER")
                            return HttpResponse(json.dumps(Res), content_type="application/json")
                        if float(price) <= 0:
                            Res = Response(-1130, "INVALID_PARAMETER")
                            return HttpResponse(json.dumps(Res), content_type="application/json")
                        if "." in str(price):
                            if len(price.split(".")[1]) > quoteprn:
                                Res = Response(-1130, "INVALID_PARAMETER")
                                return HttpResponse(json.dumps(Res), content_type="application/json")
                        if float(price) > maxprice or float(price) < minprice:
                            Res = Response(-900, "Filter failure: PRICE_FILTER")
                            return HttpResponse(json.dumps(Res), content_type="application/json")
                        Res = limitsell(account, price, qty, symbol, memuuid, ybdbrate, nbdbrate, bdbuse, value, value2)
                        return HttpResponse(json.dumps(Res), content_type="application/json")
                    elif cate == 'market' and side == 'buy' and ('market' in statustmp):
                        Res = marketbuy(account, qty, symbol, memuuid, ybdbrate, nbdbrate, bdbuse, value, value2)
                        return HttpResponse(json.dumps(Res), content_type="application/json")
                    elif cate == 'market' and side == 'sell' and ('market' in statustmp):
                        Res = marketsell(account, qty, symbol, memuuid, ybdbrate, nbdbrate, bdbuse, value, value2)
                        return HttpResponse(json.dumps(Res), content_type="application/json")
                    else:
                        Res = Response(-1000, 'UNKNOW')
                        return HttpResponse(json.dumps(Res), content_type="application/json")
            else:
                Res = Response(-903, "this symbol can't trade")
                return HttpResponse(json.dumps(Res), content_type="application/json")
        else:
            message = 'please input data.'
        return render(request, "createorder.html", locals())

    except Exception as e:
        tp, val, tb = sys.exc_info()
        error = str(val), str(tb)
        print(str(error), str(tb.tb_lineno))
        Res = Response(-1000, "UNKNOWN")
        return HttpResponse(json.dumps(Res), content_type="application/json")


def cancelorder(request):
    try:

        test = order.objects.all()

        if request.method == "POST":
            Res = {}
            # 寫入的東西
            memuuid = request.user.id
            oid = request.POST["oid"]
            symbol = request.POST["symbol"]
            timestamp = request.POST["timestamp"]
            cancelnum = 5  # N秒內超過M次

            if request.user.cancel_num > cancelnum:
                Res = Response("error", "cancel too many times")
                return HttpResponse(json.dumps(Res), content_type="application/json")
            with transaction.atomic():
                try:
                    left = symbol.split("_")[0]
                    right = symbol.split("_")[1]
                    canceltmp = order.objects.get(memuuid=memuuid, symbol=symbol, timestamp=timestamp, oid=oid,
                                                  status=0)
                    price = float(canceltmp.price)
                    qty = float(canceltmp.qty)
                    total = float(canceltmp.total)
                    side = canceltmp.side

                    if side == 'buy':
                        refund = balance.objects.get(memuuid=memuuid, coin=right)
                        freeze = float(refund.freeze)
                        canuse = float(refund.canuse)
                        canceltmp.status = 3
                        canceltmp.save()
                        refund.freeze = float(refund.freeze) - (qty * price)
                        refund.canuse = float(refund.canuse) + (qty * price)
                        refund.save()
                        check_cancel(request.user)
                        Res = Response("ok", "success")
                        return HttpResponse(json.dumps(Res), content_type="application/json")
                    elif side == 'sell':
                        refund = balance.objects.get(memuuid=memuuid, coin=left)
                        freeze = float(refund.freeze)
                        canuse = float(refund.canuse)
                        canceltmp.status = 3
                        canceltmp.save()
                        refund.freeze = float(refund.freeze) - (qty)
                        refund.canuse = float(refund.canuse) + (qty)
                        refund.save()
                        check_cancel(request.user)
                        Res = Response("ok", "success")
                        return HttpResponse(json.dumps(Res), content_type="application/json")
                except Exception as e:
                    tp, val, tb = sys.exc_info()
                    error = str(val), str(tb)
                    print(str(error), str(tb.tb_lineno))
                    Res = Response("error", "no this order")
                    return HttpResponse(json.dumps(Res), content_type="application/json")
        return render(request, "cancelorder.html", locals())
    except Exception as e:
        tp, val, tb = sys.exc_info()
        error = str(val), str(tb)
        print(str(error), str(tb.tb_lineno))
        return render(request, "cancelorder.html", locals())


# other functions
def Response(code, mess):
    try:
        Res = {}
        Res['code'] = code
        Res['msg'] = mess
        return Res
    except Exception as e:
        tp, val, tb = sys.exc_info()
        error = str(val), str(tb)
        print(str(error), str(tb.tb_lineno))


def limitbuy(account, price, qty, symbol, memuuid, ybdbrate, nbdbrate, bdbuse, value, value2):
    Res = {}
    try:
        coin2 = symbol.split("_")[1]  # coin2 = USDT
        coin3 = symbol.split("_")[0]  # coin3 = ADA
    except:
        Response(-1121, "Invalid symbol.")
        return Res
    try:
        userinfo = balance.objects.get(coin_id=coin2, memuuid_id=memuuid)  # 先知道有多少USDT可以扣
        userinfo2 = balance.objects.get(coin_id=coin3, memuuid_id=memuuid)  # 最後要加到ADA這
        userinfo3 = balance.objects.get(coin_id="BDB", memuuid_id=memuuid)  # BDB有多少

        leftfee = fee.objects.get(coin=coin3)
        rightfee = fee.objects.get(coin=coin2)
        bdbfee = fee.objects.get(coin="BDB")

        def create(timestamp, account, side, price, qty, total, status, cate, symbol_id, memuuid_id, tradetime,
                   relatedid, commission, commission2):
            unit = order.objects.create(timestamp=timestamp, account=account, side=side, price=price, qty=qty,
                                        total=total, status=status, cate="market",
                                        symbol_id=symbol_id, memuuid_id=memuuid_id, tradetime=tradetime,
                                        relatedid=relatedid, commission=commission, commission2=commission2)
            unit.save()

        def limitBuyOrder():
            unit = order.objects.create(timestamp=time.time(), account=account, side="buy", price=price, qty=qty,
                                        total=float(price) * float(qty), status=0, cate="limit",
                                        symbol_id=symbol, memuuid_id=memuuid)
            unit.save()
            userinfo.canuse = userinfo.canuse - float(price) * float(qty)
            userinfo.freeze = userinfo.freeze + float(price) * float(qty)
            userinfo.save()
            Res = Response("ok", "success")
            return Res

        money = userinfo.canuse  # 先知道有多少USDT可以扣

        with transaction.atomic():

            try:  # 這個try是避免沒有找到符合條件的
                selllist2 = \
                order.objects.filter(side="sell", symbol_id=symbol, status=0).filter(~Q(memuuid_id=memuuid)).order_by(
                    "price")[0]  # 單中最低賣
                if (float(money) > 0) and (float(qty) * float(price) <= float(money)):  # 有錢 + 錢夠花
                    if float(price) >= selllist2.price:  # 如果至少有得買

                        selllist = order.objects.filter(price__lte=price, side="sell", symbol_id=symbol,
                                                        status=0).filter(~Q(memuuid_id=memuuid)).order_by("price",
                                                                                                          "-timestamp")
                        for i in selllist:  # 從最低的單開始
                            try:  # 這個try是預防get不到東西
                                sellerinfo = balance.objects.get(coin_id=coin2,
                                                                 memuuid_id=i.memuuid_id)  # 處理掛單user的USDT
                                sellerinfo2 = balance.objects.get(coin_id=coin3,
                                                                  memuuid_id=i.memuuid_id)  # 處理掛單user的ADA
                                sellerinfo3 = balance.objects.get(coin_id="BDB",
                                                                  memuuid_id=i.memuuid_id)  # 處理掛單user的BDB
                                buyerbdb = check_usebdb(i.account)
                                if float(price) - i.price >= 0.0:  # 如果想要的金額比最低這張高
                                    relatedid = relatedID()
                                    if float(qty) >= i.qty:  # 買得起且買的數量可以清掉掛買單 ex: 想買1000, 此單只有400
                                        qty = float(qty) - i.qty  # 想買變成600
                                        money = money - (i.price) * (i.qty)  # 可用價 - 400 (USDT)
                                        userinfo.canuse = money  # 可用價更新 (USDT 減少)
                                        userinfo.total = float(userinfo.total) - (i.price) * (i.qty)  # 總資產更新 (USDT 減少)
                                        userinfo.save()

                                        sellerinfo2.total = sellerinfo2.total - float(i.qty)  # ADA total -- 減少數量
                                        sellerinfo2.freeze = sellerinfo2.freeze - float(i.qty)  # freeze --
                                        sellerinfo2.save()

                                        if not buyerbdb:  # order no bdb
                                            # no bdb dicount
                                            buyerdiscount = i.memuuid.level.discount
                                            # order usdt ++
                                            sellerinfo.total = sellerinfo.total + i.qty * i.price * (1 - buyerdiscount)
                                            sellerinfo.canuse = sellerinfo.canuse + i.qty * i.price * (
                                                        1 - buyerdiscount)
                                            sellerinfo.save()
                                            # fee ++
                                            rightfee.total = rightfee.total + i.qty * i.price * buyerdiscount
                                            rightfee.save()

                                            i.relatedid = relatedid
                                            i.commission2 = i.qty * i.price * buyerdiscount
                                            i.status = 1
                                            i.save()
                                        # 單有開BDB
                                        else:
                                            # 沒開
                                            buyerdiscount = i.memuuid.level.discount
                                            # 有開
                                            buyerdiscount2 = i.memuuid.level.bdbdiscount
                                            # BDB可支付USDT
                                            if sellerinfo3.canuse >= float(i.qty) * float(
                                                    i.price) * value2 * buyerdiscount2:
                                                # 單上的USDT都會全拿
                                                sellerinfo.total = sellerinfo.total + float(i.qty) * float(i.price)
                                                sellerinfo.canuse = sellerinfo.canuse + float(i.qty) * float(i.price)
                                                sellerinfo.save()
                                                # 扣BDB
                                                sellerinfo3.total = sellerinfo3.total - float(i.qty) * float(
                                                    i.price) * value2
                                                sellerinfo3.canuse = sellerinfo3.canuse - float(i.qty) * float(
                                                    i.price) * value2
                                                sellerinfo3.save()
                                                # BDB FEE++
                                                bdbfee.total = bdbfee.total + float(i.qty) * float(i.price) * value2
                                                bdbfee.save()
                                                # commission
                                                i.commission = float(i.qty) * float(i.price) * value2
                                                i.status = 1
                                                i.save()
                                            # BDB不夠付
                                            else:
                                                # 先把val換成bdb再扣掉自己的
                                                val = float(i.qty) * float(
                                                    i.price) * value * buyerdiscount2 - sellerinfo3.canuse
                                                val2 = val / value / buyerdiscount2
                                                # bdb fee ++
                                                bdbfee.total = bdbfee.total + sellerinfo3.canuse
                                                bdbfee.save()
                                                # commission
                                                i.commission = sellerinfo3.canuse
                                                i.status = 1
                                                i.save()
                                                # 手頭歸零
                                                sellerinfo3.total = sellerinfo3.total - sellerinfo3.canuse
                                                sellerinfo3.canuse = 0
                                                sellerinfo3.save()
                                                # usdt
                                                sellerinfo.canuse = sellerinfo.canuse + float(i.qty) * float(
                                                    i.price) - val2 * buyerdiscount
                                                sellerinfo.total = sellerinfo.total + float(i.qty) * float(
                                                    i.price) - val2 * buyerdiscount
                                                sellerinfo.save()
                                                # usdt fee ++
                                                rightfee.total = rightfee.total + val2 * buyerdiscount
                                                rightfee.save()
                                        # if user didnt use bdb
                                        if ybdbrate == 0:
                                            # user +ADA -fee
                                            userinfo2.total = userinfo2.total + float(i.qty) * (1 - nbdbrate)
                                            userinfo2.canuse = userinfo2.canuse + float(i.qty) * (1 - nbdbrate)
                                            userinfo2.save()
                                            # ADA fee ++
                                            leftfee.total = leftfee.total + float(i.qty) * nbdbrate
                                            leftfee.save()

                                            create(time.time(), account, "buy", i.price, i.qty,
                                                   float(i.price) * float(i.qty), 2, "limit", symbol, memuuid,
                                                   time.time(), relatedid, 0, float(i.qty) * nbdbrate)
                                        # if user use bdb
                                        else:
                                            # BDB 夠支付
                                            if userinfo3.canuse >= float(i.qty) * value * ybdbrate:
                                                # BDB 支付
                                                userinfo3.canuse = userinfo3.canuse - float(i.qty) * value * ybdbrate
                                                userinfo3.total = userinfo3.total - float(i.qty) * value * ybdbrate
                                                userinfo3.save()
                                                # BDB fee++
                                                bdbfee.total = bdbfee.total + float(i.qty) * value * ybdbrate
                                                bdbfee.save()
                                                # ADA 全拿
                                                userinfo2.total = userinfo2.total + float(i.qty)
                                                userinfo2.canuse = userinfo2.canuse + float(i.qty)
                                                userinfo2.save()

                                                create(time.time(), account, "buy", i.price, qty,
                                                       float(i.price) * float(i.qty), 2, "limit", symbol, memuuid,
                                                       time.time(), relatedid, float(i.qty) * ybdbrate * value, 0)
                                            # 不夠付
                                            else:
                                                # 算出不夠付的BDB (ADA)
                                                val = float(i.qty) * value * ybdbrate - sellerinfo3.canuse
                                                # BDB FEE++
                                                bdbfee.total = bdbfee.total + sellerinfo3.canuse
                                                bdbfee.save()
                                                # 手頭=0
                                                userinfo3.total = userinfo3.total - userinfo3.canuse
                                                userinfo3.canuse = 0
                                                userinfo3.save()
                                                # 把不夠付的轉換再付 ADA->BDB
                                                val2 = val / value / ybdbrate
                                                # USER ++ADA  -FEE
                                                userinfo2.total = userinfo2.total + float(i.qty) - val2 * nbdbrate
                                                userinfo2.canuse = userinfo2.canuse + float(i.qty) - val2 * nbdbrate
                                                userinfo2.save()
                                                # ADA FEE ++
                                                leftfee.total = leftfee.total + val2 * nbdbrate
                                                leftfee.save()

                                                create(time.time(), account, "buy", i.price, qty,
                                                       float(i.price) * float(i.qty), 2, "limit", symbol, memuuid,
                                                       time.time(), relatedid, userinfo3.canuse, val2 * nbdbrate)
                                        if qty == 0:
                                            break


                                    else:  # 想要1000，但單上有1500 => 拆成1000成交 + 500未成交
                                        # 買不到的部分
                                        i.qty = i.qty - float(qty)
                                        i.total = i.qty * float(i.price)
                                        i.save()

                                        sellerinfo2.total = sellerinfo2.total - float(qty)  # 結清freeze
                                        sellerinfo2.freeze = sellerinfo2.freeze - float(qty)  # freeze --
                                        sellerinfo2.save()
                                        money = money - (float(qty) * float(i.price))
                                        userinfo.canuse = float(userinfo.canuse) - (i.price) * float(qty)  # 付錢
                                        userinfo.total = float(userinfo.total) - (i.price) * float(qty)  # 付錢
                                        userinfo.save()
                                        if not buyerbdb:  # 如果單上的人沒有開BDB
                                            # no bdb discount
                                            buyerdiscount = i.memuuid.level.discount
                                            # 拿到實際的(1- n%)
                                            sellerinfo.total = sellerinfo.total + float(qty) * float(i.price) * (
                                                        1 - buyerdiscount)
                                            sellerinfo.canuse = sellerinfo.canuse + float(qty) * float(i.price) * (
                                                        1 - buyerdiscount)
                                            sellerinfo.save()
                                            # fee ++n%
                                            rightfee.total = rightfee.total + float(qty) * float(
                                                i.price) * buyerdiscount
                                            rightfee.save()
                                            create(time.time(), i.account, "sell", i.price, qty,
                                                   float(i.price) * float(qty), 1, "limit", symbol, i.memuuid_id,
                                                   time.time(), relatedid, 0,
                                                   float(qty) * float(i.price) * buyerdiscount)
                                        # order user use dbd
                                        else:
                                            buyerdiscount2 = i.memuuid.level.bdbdiscount
                                            buyerdiscount = i.memuuid.level.discount
                                            # 如果BDB夠抵銷
                                            if sellerinfo3.canuse >= float(qty) * float(i.price) * value2 * ybdbrate:
                                                # 拿100% USDT
                                                sellerinfo.total = sellerinfo.total + float(qty) * float(i.price)
                                                sellerinfo.canuse = sellerinfo.canuse + float(qty) * float(i.price)
                                                sellerinfo.save()
                                                # -BDB
                                                sellerinfo3.total = sellerinfo3.total - float(qty) * float(
                                                    i.price) * value2
                                                sellerinfo3.canuse = sellerinfo3.canuse - float(qty) * float(
                                                    i.price) * value2
                                                sellerinfo3.save()
                                                # BDB FEE++
                                                bdbfee.total = bdbfee.total + float(qty) * float(i.price) * value2
                                                bdbfee.save()

                                                create(time.time(), i.account, "sell", i.price, qty,
                                                       float(i.price) * float(qty), 1, "limit", symbol, i.memuuid_id,
                                                       time.time(), relatedid, float(qty) * float(i.price) * value2, 0)
                                            # 不夠抵
                                            else:
                                                # USDT換BDB - 手頭BDB
                                                val = float(qty) * float(price) * value2 * ybdbrate - sellerinfo3.canuse
                                                # 不夠付的BDB變USDT
                                                val2 = val / value2 / ybdbrate
                                                # bdbfee ++
                                                bdbfee.total = bdbfee.total + sellerinfo3.canuse
                                                bdbfee.save()
                                                # 手頭BDB歸零
                                                sellerinfo3.total = sellerinfo3.total - sellerinfo3.canuse
                                                sellerinfo3.canuse = 0
                                                sellerinfo3.save()
                                                # 拿的USDT要扣FEE
                                                sellerinfo.total = sellerinfo.total + float(price) * float(
                                                    qty) - val2 * buyerdiscount
                                                sellerinfo.canuse = sellerinfo.canuse + float(price) * float(
                                                    qty) - val2 * buyerdiscount
                                                sellerinfo.save()
                                                # usdt fee ++
                                                rightfee.total = rightfee.total + val2 * buyerdiscount
                                                rightfee.save()
                                                # 生成名單
                                                create(time.time(), i.account, "sell", i.price, qty,
                                                       float(i.price) * float(qty), 1, "limit", symbol, i.memuuid_id,
                                                       time.time(), relatedid, sellerinfo3.canuse, val2 * buyerdiscount)
                                        # user didnt use bdb
                                        if ybdbrate == 0:
                                            # 沒BDB可以扣 所以直接少得ADA
                                            userinfo2.total = userinfo2.total + float(qty) * (1 - nbdbrate)
                                            userinfo2.canuse = userinfo2.canuse + float(qty) * (1 - nbdbrate)
                                            userinfo2.save()
                                            # ADAfee ++
                                            leftfee.total = leftfee.total + float(qty) * nbdbrate
                                            leftfee.save()

                                            create(time.time(), account, "buy", i.price, qty,
                                                   float(i.price) * float(qty), 2, "limit", symbol, i.memuuid_id,
                                                   time.time(), relatedid, 0, float(qty) * nbdbrate)
                                        # user used bdb
                                        else:
                                            # BDB enough (ADA)
                                            if userinfo3.canuse >= float(qty) * value * ybdbrate:
                                                # user BDB --
                                                userinfo3.total = userinfo3.total - float(qty) * value * ybdbrate
                                                userinfo3.canuse = userinfo3.canuse - float(qty) * value * ybdbrate
                                                userinfo3.save()
                                                # fee++
                                                bdbfee.total = bdbfee.total + float(qty) * value * ybdbrate
                                                bdbfee.save()
                                                # user ADA increase 100%
                                                userinfo2.total = userinfo2.total + float(qty)
                                                userinfo2.canuse = userinfo2.canuse + float(qty)
                                                userinfo2.save()

                                                create(time.time(), account, "buy", i.price, qty,
                                                       float(i.price) * float(qty), 2, "market", symbol, memuuid,
                                                       time.time(), relatedid, float(qty) * value * ybdbrate, 0)
                                            # bdb is not enough
                                            else:
                                                # ADA轉換成BDB 扣掉 手上有的
                                                val = float(qty) * value * ybdbrate - userinfo3.canuse
                                                # 看還有多少ADA沒扣
                                                val2 = val / value / ybdbrate
                                                # fee ++
                                                bdbfee.total = bdbfee.total + userinfo3.canuse
                                                bdbfee.save()

                                                create(time.time(), account, "buy", i.price, qty,
                                                       float(i.price) * float(qty), 2, "market", symbol, memuuid,
                                                       time.time(), relatedid, userinfo3.canuse, val2 * nbdbrate)
                                                # user bdb = 0
                                                userinfo3.total = userinfo3.total - userinfo3.canuse
                                                userinfo3.canuse = 0
                                                userinfo3.save()
                                                # ADA ++   -fee
                                                userinfo2.canuse = userinfo2.canuse + float(qty) - val2 * nbdbrate
                                                userinfo2.total = userinfo2.total + float(qty) - val2 * nbdbrate
                                                userinfo2.save()
                                                # fee ++
                                                leftfee.total = leftfee.total + val2 * nbdbrate
                                                leftfee.save()
                                        qty = 0
                                        Res = Response("ok", "success")
                                        return Res
                                    if qty == 0:
                                        Res = Response("ok", "success")
                                        return Res
                                else:  # 最低的單價錢>限制的金額，掛買單
                                    Res = limitBuyOrder()
                                    return Res
                            except Exception as e:
                                tp, val, tb = sys.exc_info()
                                error = str(val), str(tb)
                                print(str(error), str(tb.tb_lineno))
                                break
                        if float(qty) > 0:
                            Res = limitBuyOrder()
                            return Res

                        if float(qty) == 0:
                            Res = Response("ok", "success")
                            return Res

                    else:
                        Res = limitBuyOrder()
                        return Res
                else:
                    Res = Response(-1010, "Account has insufficient balance for requested action")
                    return Res
            except:

                if (float(money) > 0) and (float(qty) * float(price) <= float(money)):  # 有錢 + 錢夠花

                    unit3 = order.objects.create(timestamp=time.time(), account=account, side="buy", price=price,
                                                 qty=qty, total=float(price) * float(qty), status=0, cate="limit",
                                                 symbol_id=symbol, memuuid_id=memuuid)
                    unit3.save()
                    userinfo.canuse = userinfo.canuse - float(price) * float(qty)
                    userinfo.freeze = userinfo.freeze + float(price) * float(qty)
                    userinfo.save()
                    Res = Response("ok", "success")
                    return Res
                else:
                    Res = Response(-1010, "Account has insufficient balance for requested action")
                    return Res
    except Exception as e:
        tp, val, tb = sys.exc_info()
        error = str(val), str(tb)
        print(str(error), str(tb.tb_lineno))
        Res = Response(-1000, "UNKNOWN")
        return Res


def limitsell(account, price, qty, symbol, memuuid, ybdbrate, nbdbrate, bdbuse, value, value2):
    # print(account, price, qty, symbol, memuuid, ybdbrate, nbdbrate, bdbuse, value, value2)
    Res = {}
    try:
        coin2 = symbol.split("_")[1]  # coin2 = USDT
        coin3 = symbol.split("_")[0]  # coin3 = ADA可用
    except:
        Response(-1121, "Invalid symbol.")
        return Res
    try:
        # userinfo = balance.objects.get(coin = coin2, memuuid_id = memuuid)  #先知道有多少錢
        userinfo = balance.objects.get(coin_id=coin2, memuuid_id=memuuid)  # 最後要加到USDT這
        userinfo2 = balance.objects.get(coin_id=coin3, memuuid_id=memuuid)  # 先知道有多少ADA可以扣
        userinfo3 = balance.objects.get(coin_id="BDB", memuuid_id=memuuid)  # BDB有多少

        leftfee = fee.objects.get(coin=coin3)
        rightfee = fee.objects.get(coin=coin2)
        bdbfee = fee.objects.get(coin="BDB")

        def limitSellOrder():
            unit = order.objects.create(timestamp=time.time(), account=account, side="sell", price=price, qty=qty,
                                        total=float(price) * float(qty), status=0, cate="limit",
                                        symbol_id=symbol, memuuid_id=memuuid)
            unit.save()
            userinfo2.canuse = userinfo2.canuse - float(qty)  # ADA可用 下降
            userinfo2.freeze = userinfo2.freeze + float(qty)  # ADA凍結 上升
            userinfo2.save()
            Res = Response("ok", "success")
            return Res

        def create(timestamp, account, side, price, qty, total, status, cate, symbol_id, memuuid_id, tradetime,
                   relatedid, commission, commission2):
            unit = order.objects.create(timestamp=timestamp, account=account, side=side, price=price, qty=qty,
                                        total=total, status=status, cate="market",
                                        symbol_id=symbol_id, memuuid_id=memuuid_id, tradetime=tradetime,
                                        relatedid=relatedid, commission=commission, commission2=commission2)
            unit.save()

        with transaction.atomic():
            money = userinfo2.canuse  # ADA
            try:  # 這個try防filter不到, 發生時也表示沒單能交易
                selllist2 = \
                order.objects.filter(side="buy", symbol_id=symbol, status=0).filter(~Q(memuuid_id=memuuid)).order_by(
                    "-price")[0]

                if float(price) <= selllist2.price:

                    if (float(money) > 0) and (float(qty) * float(price) <= float(money)):
                        buylist = order.objects.filter(price__gte=price, side="buy", symbol_id=symbol, status=0).filter(
                            ~Q(memuuid_id=memuuid)).order_by("-price", "-timestamp")
                        for i in buylist:  # 從最高的單開始
                            try:  # 防get沒東西
                                buyerinfo = balance.objects.get(coin_id=coin3, memuuid_id=i.memuuid_id)  # 處理掛單user的ADA
                                buyerinfo2 = balance.objects.get(coin_id=coin2,
                                                                 memuuid_id=i.memuuid_id)  # 處理掛單user的USDT
                                buyerinfo3 = balance.objects.get(coin_id="BDB", memuuid_id=i.memuuid_id)  # 處理掛單user的BDB

                                buyerbdb = check_usebdb(i.account)

                                if float(price) - i.price <= 0.0:  # 如果想要的金額比最低這張高
                                    relatedid = relatedID()
                                    if float(qty) >= i.qty:  # 買得起且買的數量可以清掉掛買單 ex: 想買1000, 此單只有400
                                        qty = float(qty) - i.qty  # 想買變成600
                                        money = money - (i.price) * (i.qty)  # 可用價ADA - 400
                                        userinfo2.canuse = float(userinfo.canuse) - (i.qty)  # 可用價ADA更新(減少)
                                        userinfo2.total = float(userinfo.total) - (i.qty)  # 總資產更新(ADA減少)
                                        userinfo2.save()
                                        buyerinfo2.total = buyerinfo2.total - (i.price) * (i.qty)
                                        buyerinfo2.freeze = buyerinfo2.freeze - (i.price) * (i.qty)

                                        buyerinfo2.save()

                                        if not buyerbdb:  # 如果單上的人沒有開BDB

                                            # 沒BDB的折扣
                                            buyerdiscount = i.memuuid.level.discount
                                            # 單子上的ADA增加
                                            buyerinfo.total = buyerinfo.total + (i.qty) * (1 - buyerdiscount)
                                            buyerinfo.canuse = buyerinfo.canuse + (i.qty) * (1 - buyerdiscount)
                                            buyerinfo.save()
                                            leftfee.total = leftfee.total + i.qty * buyerdiscount
                                            leftfee.save()
                                            i.commission2 = (i.qty) * buyerdiscount
                                            i.status = 1

                                            i.save()
                                        # create(time.time(), i.account, "buy", i.price, qty, float(i.price)*float(i.qty), 1, "limit", symbol, i.memuuid_id, time.time(), relatedid, 0, (float(qty)) * buyerdiscount)

                                        # 有開BDB
                                        else:

                                            # 沒開
                                            buyerdiscount = i.memuuid.level.discount
                                            # 有開
                                            buyerdiscount2 = i.memuuid.level.bdbdiscount
                                            # 如果BDB可以支付完ADA轉換的BDB
                                            if buyerinfo3.canuse >= float(i.qty) * value2 * buyerdiscount2:

                                                # 單上的ADA都會全拿
                                                buyerinfo.total = buyerinfo.total + float(i.qty)
                                                buyerinfo.canuse = buyerinfo.canuse + float(i.qty)
                                                buyerinfo.save()
                                                # 只扣BDB
                                                buyerinfo3.total = buyerinfo3.total - float(i.qty) * value2
                                                buyerinfo3.canuse = buyerinfo3.canuse - float(i.qty) * value2
                                                buyerinfo3.save()
                                                # BDBFEE++
                                                bdbfee.total = bdbfee.total - float(i.qty) * value2
                                                bdbfee.save()
                                                # commission
                                                i.commission = float(i.qty) * value2
                                                i.status = 1
                                                i.save()
                                            # create(time.time(), i.account, "buy", i.price, qty, float(i.price)*float(qty), 1, "limit", symbol, i.memuuid_id, time.time(), relatedid, 0, (float(qty)) * buyerdiscount)
                                            # BDB不夠付
                                            else:
                                                # val = ADA換成BDB手續費 - 自己剩的
                                                val = float(i.qty) * value2 * buyerdiscount2 - buyerinfo3.canuse
                                                val2 = val / value2 / buyerdiscount2
                                                # bdb fee ++
                                                bdbfee.total = bdbfee.total + buyerinfo3.canuse
                                                bdbfee.save()
                                                # cmmission++
                                                i.commission = buyerinfo3.canuse
                                                i.status = 1
                                                i.save()
                                                # 手頭歸零
                                                buyerinfo3.total = buyerinfo3.total - buyerinfo3.canuse
                                                buyerinfo3.canuse = 0
                                                buyerinfo3.save()
                                                # 拿ADA數量-手續
                                                buyerinfo.canuse = buyerinfo.canuse + (i.qty) - (val2 * buyerdiscount)
                                                buyerinfo.total = buyerinfo.total + (i.qty) - (val2 * buyerdiscount)
                                                buyerinfo.save()
                                                # fee++
                                                leftfee.total = leftfee.total + (val2 * buyerdiscount)
                                                leftfee.save()
                                            # create(time.time(), i.account, "buy", i.price, qty, float(i.price)*float(qty), 1, "limit", symbol, i.memuuid_id, time.time(), relatedid, buyerinfo3.canuse, (val2 * buyerdiscount))
                                        # if user didnt use bdb
                                        if ybdbrate == 0:

                                            # user usdt++
                                            userinfo.total = userinfo.total + float(i.qty) * float(i.price) * (
                                                        1 - nbdbrate)
                                            userinfo.canuse = userinfo.canuse + float(i.qty) * float(i.price) * (
                                                        1 - nbdbrate)
                                            userinfo.save()
                                            # usdt fee ++
                                            rightfee.total = rightfee.total + float(i.price) * float(i.qty) * (nbdbrate)
                                            rightfee.save()
                                            create(time.time(), account, "sell", i.price, i.qty,
                                                   float(i.price) * float(i.qty), 2, "limit", symbol, memuuid,
                                                   time.time(), relatedid, 0,
                                                   (float(qty)) * float(price) * buyerdiscount)
                                        # user use bdb
                                        else:

                                            # BDB夠付ADA
                                            if userinfo3.canuse >= float(i.price) * float(i.qty) * ybdbrate * value:

                                                # BDB支付
                                                userinfo3.total = userinfo3.total - float(i.price) * float(
                                                    i.qty) * ybdbrate * value
                                                userinfo3.canuse = userinfo3.canuse - float(i.price) * float(
                                                    i.qty) * ybdbrate * value
                                                userinfo3.save()
                                                # BDB FEE ++
                                                bdbfee.total = bdbfee.total + float(i.price) * float(
                                                    i.qty) * ybdbrate * value
                                                bdbfee.save()
                                                # 收到的USDT全拿
                                                userinfo.total = userinfo.total + float(i.price) * float(i.qty)
                                                userinfo.canuse = userinfo.canuse + float(i.price) * float(i.qty)
                                                userinfo.save()
                                                create(time.time(), account, "sell", i.price, qty,
                                                       float(i.price) * float(i.qty), 2, "limit", symbol, memuuid,
                                                       time.time(), relatedid,
                                                       float(i.price) * float(i.qty) * ybdbrate * value, 0)
                                            # BDB 不夠付
                                            else:

                                                # 不夠付的BDB
                                                val = float(i.qty) * value2 * ybdbrate - userinfo3.canuse
                                                # 把已支付的收起來
                                                bdbfee.total = bdbfee.total + userinfo3.canuse
                                                bdbfee.save()
                                                # 把擁有的BDB=0
                                                userinfo3.total = userinfo3.total - userinfo3.canuse
                                                userinfo3.canuse = 0
                                                userinfo3.save()
                                                # 把不夠付的BDB轉成USDT
                                                val2 = val / value2 / ybdbrate
                                                # USDT拿了要扣掉
                                                userinfo.total = userinfo.total + float(i.qty) * float(
                                                    i.price) - val2 * nbdbrate
                                                userinfo.canuse = userinfo.canuse + float(i.qty) * float(
                                                    i.price) - val2 * nbdbrate
                                                userinfo.save
                                                # usdt fee ++
                                                rightfee.total = rightfee.total + val2 * nbdbrate
                                                rightfee.save()
                                                create(time.time(), account, "sell", i.price, qty,
                                                       float(i.price) * float(i.qty), 2, "limit", symbol, memuuid,
                                                       time.time(), relatedid, userinfo3.canuse, val2 * nbdbrate)

                                        if qty == 0:
                                            break

                                    else:  # 想要1000，但單上有1500 => 拆成1000成交 + 500未成交
                                        # 碰不完的部分
                                        i.qty = i.qty - float(qty)
                                        i.total = i.qty * float(i.price)
                                        i.save()
                                        # 可完成ㄉ部分

                                        money = money - (float(qty) * float(i.price))  # 減少 ADAtmp
                                        userinfo2.canuse = float(userinfo2.canuse) - (i.price) * float(
                                            qty)  # 手上的ADA減少了可以交易的數量 * 單子上的價錢
                                        userinfo2.total = float(userinfo2.total) - (i.price) * float(qty)  # 手上的ADA 減少
                                        userinfo2.save()
                                        buyerinfo2.total = buyerinfo2.total - float(i.price) * float(qty)
                                        buyerinfo2.freeze = buyerinfo2.freeze - float(i.price) * float(qty)

                                        buyerinfo2.save()

                                        if not buyerbdb:  # 如果單上的人沒有開BDB

                                            buyerdiscount = i.memuuid.level.discount
                                            # 拿到實際的(1- n%)
                                            buyerinfo2.total = buyerinfo2.total + (float(qty)) * (
                                                        1 - buyerdiscount)  # 單上的人會+ADA 數量(qty)
                                            buyerinfo2.canuse = buyerinfo2.canuse + (float(qty)) * (
                                                        1 - buyerdiscount)  # 單上的人會+ADA 數量(qty)
                                            buyerinfo2.save()
                                            # 把n%收起來
                                            leftfee.total = leftfee.total + (float(qty)) * buyerdiscount
                                            leftfee.save()
                                            # 生成一筆自己的單
                                            create(time.time(), i.account, "buy", i.price, qty,
                                                   float(i.price) * float(qty), 1, "limit", symbol, i.memuuid_id,
                                                   time.time(), relatedid, 0, (float(qty)) * buyerdiscount)


                                        else:

                                            buyerdiscount2 = i.memuuid.level.bdbdiscount
                                            buyerdiscount = i.memuuid.level.discount
                                            if buyerinfo3.canuse >= (float(qty)) * buyerdiscount2 * value2:  # 如果BDB夠抵銷

                                                # 實際拿多少就多少 只扣bdb
                                                buyerinfo.total = buyerinfo.total + (float(qty))  # 單上的人會+ADA 單數量(qty)
                                                buyerinfo.canuse = buyerinfo.canuse + (float(qty))  # 單上的人會+ADA 單數量(qty)
                                                buyerinfo.save()
                                                # bdb會--
                                                buyerinfo3.total = buyerinfo3.total - (
                                                    float(qty)) * buyerdiscount2 * value2
                                                buyerinfo3.canuse = buyerinfo3.canuse - (
                                                    float(qty)) * buyerdiscount2 * value2
                                                buyerinfo3.save()
                                                # (bdb)FEE++
                                                bdbfee.total = bdbfee.total + (float(qty)) * buyerdiscount2 * value2
                                                bdbfee.save()
                                                # 生成一筆自己的單
                                                create(time.time(), i.account, "buy", i.price, qty,
                                                       float(i.price) * float(qty), 1, "limit", symbol, i.memuuid_id,
                                                       time.time(), relatedid, (float(qty)) * buyerdiscount2 * value2,
                                                       0)

                                            else:

                                                # 數量 * bdb折扣 * 價值2 - buyerinfo3.canuse
                                                val = (float(
                                                    qty)) * buyerdiscount2 * value2 - buyerinfo3.canuse  # 不夠付的BDB
                                                # 數量 / 折扣 / 價值 = 不夠付的ada
                                                val2 = val / buyerdiscount2 / value2  # 轉成原本不夠付的coin
                                                # 把原本不夠的bdb存進去
                                                bdbfee.total = bdbfee.total + buyerinfo3.canuse
                                                bdbfee.save()
                                                # 原本的bdb歸零
                                                buyerinfo3.total = buyerinfo3.total - buyerinfo3.canuse
                                                buyerinfo3.canuse = 0
                                                buyerinfo3.save()
                                                # 拿ada實際拿的再減掉手續費val2 * (buyerdiscount)
                                                buyerinfo.total = buyerinfo.total + float(qty) - val2 * (buyerdiscount)
                                                buyerinfo.canuse = buyerinfo.canuse + float(qty) - val2 * (
                                                    buyerdiscount)
                                                buyerinfo.save()
                                                # fee存進去val2 * (1 - buyerdiscount)
                                                leftfee.total = leftfee.total + val2 * buyerdiscount
                                                leftfee.save()
                                                # 生成一筆自己的單
                                                create(time.time(), i.account, "buy", i.price, qty,
                                                       float(i.price) * float(qty), 1, "limit", symbol, i.memuuid_id,
                                                       time.time(), relatedid, buyerinfo3.canuse, val2 * buyerdiscount)

                                        if ybdbrate == 0:  # no bdb

                                            # BDB不扣 只扣自己得到的
                                            userinfo.total = userinfo.total + float(qty) * float(i.price) * (
                                                        1 - nbdbrate)  # 發起交易的+USDT
                                            userinfo.canuse = userinfo.canuse + float(qty) * float(i.price) * (
                                                        1 - nbdbrate)  # 發起交易的+USDT
                                            userinfo.save()
                                            rightfee.total = rightfee.total + float(qty) * float(i.price) * (nbdbrate)
                                            rightfee.save()

                                            create(time.time(), account, "sell", i.price, qty,
                                                   float(i.price) * float(qty), 2, "limit", symbol, memuuid,
                                                   time.time(), relatedid, 0, float(qty) * float(i.price) * (nbdbrate))


                                        else:  # use bdb  # * value -> coin to bdb

                                            # 如果BDB夠支付
                                            if userinfo3.canuse >= float(qty) * float(i.price) * ybdbrate * value:

                                                # 只用BDB支付(USDT) float(qty) * float(i.price) * ybdbrate * value
                                                userinfo3.total = userinfo3.total - float(qty) * float(
                                                    i.price) * ybdbrate * value
                                                userinfo3.canuse = userinfo3.canuse - float(qty) * float(
                                                    i.price) * ybdbrate * value
                                                userinfo3.save()
                                                # BDB存float(qty) * float(i.price) * ybdbrate * value
                                                bdbfee.total = bdbfee.total + float(qty) * float(
                                                    i.price) * ybdbrate * value
                                                bdbfee.save()
                                                # USDT全拿
                                                userinfo.total = userinfo.total + float(qty) * float(
                                                    i.price)  # 發起交易的+USDT
                                                userinfo.canuse = userinfo.canuse + float(qty) * float(
                                                    i.price)  # 發起交易的+USDT
                                                userinfo.save()
                                                # 生成一筆自己的單

                                                create(time.time(), account, "sell", i.price, qty,
                                                       float(i.price) * float(qty), 2, "market", symbol, memuuid,
                                                       time.time(), relatedid, 0,
                                                       float(qty) * float(i.price) * ybdbrate * value)

                                            else:  # bdb not enough

                                                # BDB付不夠 先知道缺多少BDB (這單多少 - 剩多少 = 還差多少)
                                                val = float(qty) * float(
                                                    i.price) * ybdbrate * value - userinfo3.canuse  # val = 花不起的BDB ####
                                                # 把剩的存起來當作已付
                                                bdbfee.total = bdbfee.total + userinfo3.canuse
                                                bdbfee.save()
                                                # BDB除VALUE除率	 = USDT
                                                val2 = (val / value) / ybdbrate  # 剩下要扣在獲得的coin的 所以乘nbd

                                                create(time.time(), account, "sell", i.price, qty,
                                                       float(i.price) * float(qty), 2, "market", symbol, memuuid,
                                                       time.time(), relatedid, userinfo3.canuse, val2 * nbdbrate)
                                                # 花光所以歸零
                                                userinfo3.total = userinfo3.total - userinfo3.canuse
                                                userinfo3.canuse = 0
                                                userinfo3.save()
                                                # USER USDT加上全部 - 手續費
                                                userinfo.canuse = userinfo.canuse + float(qty) * float(
                                                    i.price) - val2 * nbdbrate
                                                userinfo.total = userinfo.total + float(qty) * float(
                                                    i.price) - val2 * nbdbrate
                                                userinfo.save()
                                                # 手續費
                                                rightfee.total = rightfee.total + val2 * nbdbrate
                                                rightfee.save()
                                        # buyerinfo.total = buyerinfo.total + (i.price) * float(qty) #單上的ADA增加
                                        # buyerinfo.canuse = buyerinfo.canuse + (i.price) * float(qty)
                                        # buyerinfo.save()

                                        # userinfo.total = userinfo.total + float(qty) #手上的USDT增加
                                        # userinfo.canuse = userinfo.canuse + float(qty)
                                        # userinfo.save()

                                        # unit = order.objects.create(timestamp = time.time(), account = account, side = "sell", price = i.price, qty = qty, total = float(i.price)*float(qty), status = 2, cate = "limit",
                                        # 							symbol_id = symbol, memuuid_id = memuuid, tradetime = time.time(), relatedid = relatedid)
                                        # unit.save()
                                        # unit2 = order.objects.create(timestamp = time.time(), account = i.account, side = "sell", price = i.price, qty = qty, total = float(i.price)*float(qty), status = 1, cate = i.cate,
                                        # 							symbol_id = symbol, memuuid_id = 1, tradetime = time.time(), relatedid = relatedid)
                                        # unit2.save()
                                        qty = 0

                                        Res = Response("ok", "success")

                                        return Res
                                    if qty == 0:
                                        Res = Response("ok", "success")

                                        return Res
                                else:  # 單上的都比想賣的價錢低，掛個賣單出來
                                    Res = limitSellOrder()
                                    return Res
                            except Exception as e:
                                tp, val, tb = sys.exc_info()
                                error = str(val), str(tb)
                                print(str(error), str(tb.tb_lineno))
                                break
                        if float(qty) > 0:
                            Res = limitSellOrder()
                            return Res

                        if float(qty) == 0:
                            Res = Response("ok", "success")
                            return Res


                    else:
                        Res = Response(-1010, "Account has insufficient balance for requested action")
                        return Res
                else:

                    Res = limitSellOrder()
                    return Res
            except:

                if (float(money) > 0) and (float(qty) * float(price) <= float(money)):

                    unit3 = order.objects.create(timestamp=time.time(), account=account, side="sell", price=price,
                                                 qty=qty, total=float(price) * float(qty), status=0, cate="limit",
                                                 symbol_id=symbol, memuuid_id=memuuid)
                    unit3.save()

                    userinfo2.canuse = userinfo2.canuse - float(price) * float(qty)
                    userinfo2.freeze = userinfo2.freeze + float(price) * float(qty)

                    userinfo2.save()

                    Res = Response("ok", "success")
                    return Res
                else:
                    Res = Response(-1010, "Account has insufficient balance for requested action")
                    return Res

    except Exception as e:
        tp, val, tb = sys.exc_info()
        error = str(val), str(tb)
        print(str(error), str(tb.tb_lineno))
        Res = Response(-1000, "UNKNOWN")
        return Res


def marketbuy(account, qty, symbol, memuuid, ybdbrate, nbdbrate, bdbuse, value, value2):
    try:
        qtytmp = qty
        # ex symbol = ADA_USDT
        coin2 = symbol.split("_")[1]  # coin2 = USDT
        coin3 = symbol.split("_")[0]  # coin3 = ADA
        userinfo = balance.objects.get(coin_id=coin2, memuuid_id=memuuid)  # 先知道有多少USDT可以扣
        userinfo2 = balance.objects.get(coin_id=coin3, memuuid_id=memuuid)  # 最後要加到ADA這
        userinfo3 = balance.objects.get(coin_id="BDB", memuuid_id=memuuid)  # BDB有多少
        money = userinfo.canuse  # 先知道有多少USDT可以扣

        leftfee = fee.objects.get(coin=coin3)
        rightfee = fee.objects.get(coin=coin2)
        bdbfee = fee.objects.get(coin="BDB")

        with transaction.atomic():
            # 這邊概念是: 先看錢夠不夠，然後看現在買完該數量(price * qty)要多少錢(cost)，看可用資產(money)夠不夠扣cost，若可扣(>=)再作後續交易。
            if (float(money) > 0):
                try:
                    count = order.objects.filter(side='sell', status=0, symbol_id=symbol).filter(
                        ~Q(memuuid_id=memuuid)).aggregate(Sum('qty'))
                    count = count['qty__sum']
                    selllist = order.objects.filter(side="sell", symbol_id=symbol, status=0).filter(
                        ~Q(memuuid_id=memuuid)).order_by("price", "-timestamp")  # 找出未完成交易的賣單
                    cost = 0  # 這個要記買完全部要多少錢
                    #####這邊只是為了知道買不買得起想要的總數#####
                    try:  # 這個try以防沒有單子

                        if float(count) >= float(qty):

                            relatedid = relatedID()
                            for i in selllist:  # 從最便宜的單子開始算數量
                                if float(qty) >= float(
                                        i.qty):  # ex: 想買1000 & $10，單上700 & $10 => 還要買300 & cost 累加 300 * 10
                                    qty = float(qty) - float(i.qty)
                                    cost = cost + (float(i.qty) * float(i.price))  # 這邊的cost就是成交額
                                else:  # 要的數量 < 單上 ex: 想要買200 單上1000 & 10$ => 只累加cost 200(qty) * 10(單上)
                                    cost = cost + float(qty) * float(i.price)  # 這邊的cost就是成交額
                                    qty = 0
                            if (cost) <= float(money):  # 真的買得起的話再交易, 這的cost是總需花費

                                def create(timestamp, account, side, price, qty, total, status, cate, symbol_id,
                                           memuuid_id, tradetime, relatedid, commission, commission2):
                                    unit = order.objects.create(timestamp=timestamp, account=account, side=side,
                                                                price=price, qty=qty, total=total, status=status,
                                                                cate="market",
                                                                symbol_id=symbol_id, memuuid_id=memuuid_id,
                                                                tradetime=tradetime, relatedid=relatedid,
                                                                commission=commission, commission2=commission2)
                                    unit.save()

                                qty = qtytmp
                                for i in selllist:  # 從最便宜的單子開始算數量
                                    sellerinfo = balance.objects.get(coin_id=coin2,
                                                                     memuuid_id=i.memuuid_id)  # 處理掛單user的USDT (+)
                                    sellerinfo2 = balance.objects.get(coin_id=coin3,
                                                                      memuuid_id=i.memuuid_id)  # 處理掛單user的ADA (-)
                                    sellerinfo3 = balance.objects.get(coin_id="BDB",
                                                                      memuuid_id=i.memuuid_id)  # 處理掛單user的BDB
                                    buyerbdb = check_usebdb(i.account)
                                    if float(qty) >= float(
                                            i.qty):  # ex: 想買1000，單上700 & $10 => 還要買300  ， 此時要生成一個交易成功700(i.qty) + 原本單上的要改成成功
                                        qty = float(qty) - float(i.qty)  # 還要買300
                                        i.status = 1
                                        i.tradetime = time.time()
                                        i.relatedid = relatedid
                                        i.save()  # 單上的要改成成功

                                        sellerinfo2.total = sellerinfo2.total - float(i.qty)  # 單上的人會-ADA 單數量(i.qty)
                                        sellerinfo2.freeze = sellerinfo2.freeze - float(i.qty)  # 單上的人會-ADA 單數量(i.qty)
                                        sellerinfo2.save()

                                        userinfo.total = userinfo.total - float(i.price) * float(i.qty)  # 發起交易的減少USDT
                                        userinfo.canuse = userinfo.canuse - float(i.price) * float(i.qty)  # 發起交易的減少USDT
                                        userinfo.save()

                                        # 如果單上的人沒開BDB
                                        if not buyerbdb:

                                            buyerdiscount = i.memuuid.level.discount
                                            # 單上使用者的USDT TOTAL++
                                            sellerinfo.total = sellerinfo.total + float(i.qty) * float(i.price) * (
                                                        1 - buyerdiscount)
                                            # 單上使用者的USDT CANUSE ++
                                            sellerinfo.canuse = sellerinfo.canuse + float(i.qty) * float(i.price) * (
                                                        1 - buyerdiscount)
                                            sellerinfo.save()
                                            # FEE ++
                                            rightfee.total = rightfee.total + float(i.qty) * float(i.price) * (
                                                buyerdiscount)
                                            rightfee.save()

                                            i.commission2 = float(i.qty) * float(i.price) * (buyerdiscount)
                                            i.save()

                                        # 如果單上的人有開BDB
                                        else:
                                            buyerdiscount2 = i.memuuid.level.bdbdiscount  # 2 = 有開的折扣
                                            buyerdiscount = i.memuuid.level.discount  # = 正常折扣
                                            # 如果單上的BDB夠扣的話
                                            if sellerinfo3.canuse >= float(i.qty) * float(
                                                    i.price) * value2 * buyerdiscount2:

                                                # 單子人的USDT全拿
                                                sellerinfo.total = sellerinfo.total + float(i.qty) * float(i.price)
                                                sellerinfo.canuse = sellerinfo.canuse + float(i.qty) * float(i.price)
                                                sellerinfo.save()
                                                # 把BDB扣掉
                                                sellerinfo3.total = sellerinfo3.total - float(i.price) * float(
                                                    i.qty) * value2 * buyerdiscount2
                                                sellerinfo3.canuse = sellerinfo3.canuse - float(i.price) * float(
                                                    i.qty) * value2 * buyerdiscount2
                                                sellerinfo3.save()
                                                # bdbfee 收起來
                                                bdbfee.total = bdbfee.total + float(i.price) * float(
                                                    i.qty) * value2 * buyerdiscount2
                                                bdbfee.save()
                                                i.commission = float(i.price) * float(i.qty) * value2 * buyerdiscount2
                                                i.save()
                                            # 如果單上的BDB不夠扣
                                            else:

                                                # val = USDT換成BDB 扣掉自己的 = 剩下不夠支付的BDB
                                                val = float(i.price) * float(
                                                    i.qty) * buyerdiscount2 * value2 - sellerinfo3.canuse
                                                # val2 = 把剩下不夠付的BDB換回USDT
                                                val2 = val / value2 / buyerdiscount2
                                                # 把扣掉的BDB收起來
                                                bdbfee.total = bdbfee.total + sellerinfo3.canuse
                                                bdbfee.save()
                                                # 此時自己的BDB已經付光了
                                                i.commission = sellerinfo3.canuse
                                                i.save()
                                                sellerinfo3.total = sellerinfo3.total - sellerinfo3.canuse
                                                sellerinfo3.canuse = 0
                                                sellerinfo3.save()
                                                # 用USDT支付 (拿該拿的 再扣掉)
                                                sellerinfo.total = sellerinfo.total + i.qty * i.price - val2 * buyerdiscount
                                                sellerinfo.canuse = sellerinfo.canuse + i.qty * i.price - val2 * buyerdiscount
                                                sellerinfo.save()
                                                # 扣掉的裝到FEE
                                                rightfee.total = rightfee.total + val2 * buyerdiscount
                                                rightfee.save()
                                                i.commission2 = val2 * buyerdiscount
                                                i.save()

                                        ############################# for user #############################
                                        # no bdb

                                        # user didnt use bdb
                                        if ybdbrate == 0:

                                            # user 拿少點的ADA
                                            userinfo2.total = userinfo2.total + float(i.qty) * (1 - nbdbrate)
                                            userinfo2.canuse = userinfo2.canuse + float(i.qty) * (1 - nbdbrate)
                                            userinfo2.save()
                                            # 沒拿到的++fee
                                            leftfee.total = leftfee.total + float(i.qty) * (nbdbrate)
                                            leftfee.save()
                                        # 如果使用者有開BDB
                                        else:

                                            # 使用者的BDB可以支付掉BDB手續費 (ADA轉換) * BDB%數
                                            if userinfo3.canuse >= float(i.qty) * value * ybdbrate:

                                                # 只扣BDB
                                                userinfo3.total = userinfo3.total - float(i.qty) * value * ybdbrate
                                                userinfo3.canuse = userinfo3.canuse - float(i.qty) * value * ybdbrate
                                                userinfo3.save()
                                                # 扣的BDB到這
                                                bdbfee.total = bdbfee.total + float(i.qty) * value * ybdbrate
                                                bdbfee.save()
                                                # ADA全拿
                                                userinfo2.total = userinfo2.total + float(i.qty)
                                                userinfo2.canuse = userinfo2.canuse + float(i.qty)
                                                userinfo2.save()


                                            # 使用者的BDB不夠付
                                            else:

                                                # val = 把ADA先換成BDB去扣
                                                val = float(i.qty) * value * ybdbrate - userinfo3.canuse
                                                # 先收扣掉的BDB
                                                bdbfee.total = bdbfee.total + userinfo3.canuse
                                                bdbfee.save()
                                                # 把bdb歸零
                                                userinfo3.total = userinfo3.total - userinfo3.canuse
                                                userinfo3.canuse = 0
                                                userinfo3.save()
                                                # val2 = 付不了的BDB轉回ADA去扣
                                                val2 = val / value / ybdbrate
                                                # 使用者的ADA拿到比該拿的少
                                                userinfo2.canuse = userinfo2.canuse + float(i.qty) - val2 * nbdbrate
                                                userinfo2.total = userinfo2.total + float(i.qty) - val2 * nbdbrate
                                                userinfo2.save()
                                                # ADA FEE++
                                                leftfee.total = leftfee.total + val2 * nbdbrate
                                                leftfee.save()
                                        if qty == 0:
                                            break



                                    else:  # ex: 想買1000(qty)，單上1300(i.qty) => 想買0(qty) 單上300(i.qty-qty)，生成兩個成功的1000(qty)

                                        i.qty = float(i.qty) - float(qty)
                                        i.total = float(i.price) * float(i.qty)
                                        i.save()  # 單上30er0(i.qty-qty)

                                        sellerinfo2.total = sellerinfo2.total - float(qty)  # 單上的人會-ADA 數量(qty)
                                        sellerinfo2.freeze = sellerinfo2.freeze - float(qty)  # 單上的人會-ADA 數量(qty)
                                        sellerinfo2.save()
                                        userinfo.total = userinfo.total - float(i.price) * float(
                                            qty)  # 發起交易的減少USDT(單上價格(i.price) * 想買數量(qty))
                                        userinfo.canuse = userinfo.canuse - float(i.price) * float(
                                            qty)  # 發起交易的減少USDT(單上價格(i.price) * 想買數量(qty))
                                        userinfo.save()

                                        # 如果單上沒有開BDB
                                        if not buyerbdb:

                                            # 正常% USDT
                                            buyerdiscount = i.memuuid.level.discount
                                            # 抽掉USDT
                                            sellerinfo.total = sellerinfo.total + float(qty) * float(i.price) * (
                                                        1 - buyerdiscount)
                                            sellerinfo.canuse = sellerinfo.canuse + float(qty) * float(i.price) * (
                                                        1 - buyerdiscount)
                                            sellerinfo.save()
                                            # 抽掉的存起來
                                            rightfee.total = rightfee.total + float(qty) * float(i.price) * (
                                                buyerdiscount)
                                            rightfee.save()
                                            # 生成一筆自己的單
                                            create(time.time(), i.account, "sell", i.price, qty,
                                                   float(i.price) * float(qty), 1, "limit", symbol, i.memuuid_id,
                                                   time.time(), relatedid, 0, (float(qty)) * buyerdiscount)
                                        # 如果單上有開BDB:
                                        else:

                                            # 有 / 無 BDB的手續費
                                            buyerdiscount2 = i.memuuid.level.bdbdiscount
                                            buyerdiscount = i.memuuid.level.discount
                                            # 如果夠BDB夠付 BDB(USDT)
                                            if sellerinfo3.canuse >= float(qty) * float(
                                                    i.price) * value2 * buyerdiscount2:

                                                # 只扣BDB 所以USDT全拿
                                                sellerinfo.total = sellerinfo.total + float(qty) * float(i.price)
                                                sellerinfo.canuse = sellerinfo.canuse + float(qty) * float(i.price)
                                                sellerinfo.save()
                                                # 這邊扣BDB
                                                sellerinfo3.total = sellerinfo3.total - float(qty) * float(
                                                    i.price) * value2 * buyerdiscount2
                                                sellerinfo3.canuse = sellerinfo3.canuse - float(qty) * float(
                                                    i.price) * value2 * buyerdiscount2
                                                sellerinfo3.save()
                                                # BDBfee ++
                                                bdbfee.total = bdbfee.total + float(qty) * float(
                                                    i.price) * value2 * buyerdiscount2
                                                bdbfee.save()
                                                # 生成一筆自己的單
                                                create(time.time(), i.account, "sell", i.price, qty,
                                                       float(i.price) * float(qty), 1, "limit", symbol, i.memuuid_id,
                                                       time.time(), relatedid, (float(qty)) * buyerdiscount2 * value2,
                                                       0)
                                            # BDB付不夠
                                            else:

                                                # val = 把USDT先知道要多少BDB 減掉現在擁有的
                                                val = float(qty) * float(
                                                    i.price) * value2 * buyerdiscount2 - sellerinfo3.canuse
                                                # 除回去變成USDT
                                                val2 = val / value2 / buyerdiscount2
                                                # BDB FEE++
                                                bdbfee.total = bdbfee.total + sellerinfo3.canuse
                                                bdbfee.save()
                                                # 自己的BDB = 0
                                                sellerinfo3.total = sellerinfo3.total - sellerinfo3.canuse
                                                sellerinfo3.canuse = 0
                                                sellerinfo3.save()
                                                # 自己的USDT 拿比原本的少
                                                sellerinfo.total = sellerinfo.total + float(qty) * float(
                                                    i.price) - val2 * buyerdiscount
                                                sellerinfo.canuse = sellerinfo.canuse + float(qty) * float(
                                                    i.price) - val2 * buyerdiscount
                                                sellerinfo.save()
                                                # USDT fee ++
                                                rightfee.total = rightfee.total + val2 * buyerdiscount
                                                rightfee.save()
                                                # 生成一張交易單
                                                create(time.time(), i.account, "sell", i.price, qty,
                                                       float(i.price) * float(qty), 1, "limit", symbol, i.memuuid_id,
                                                       time.time(), relatedid, userinfo3.canuse, val2 * buyerdiscount)

                                        # if 使用者沒有BDB
                                        if ybdbrate == 0:

                                            # 沒有BDB 直接扣得到的ADA
                                            userinfo2.total = userinfo2.total + float(qty) * (1 - nbdbrate)
                                            userinfo2.canuse = userinfo2.canuse + float(qty) * (1 - nbdbrate)
                                            userinfo2.save()
                                            # ADA fee ++
                                            leftfee.total = leftfee.total + float(qty) * (nbdbrate)
                                            leftfee.save()
                                            # 生成訂單
                                            create(time.time(), account, "buy", i.price, qty,
                                                   float(i.price) * float(qty), 2, "market", symbol, memuuid,
                                                   time.time(), relatedid, 0, float(qty) * (nbdbrate))
                                        # 使用者有開BDB
                                        else:

                                            # IF 使用者BDB有開 夠付(ADA)
                                            if userinfo3.canuse >= float(qty) * ybdbrate * value:

                                                # 只扣BDB就好
                                                userinfo3.total = userinfo3.total - float(qty) * value * ybdbrate
                                                userinfo3.canuse = userinfo3.canuse - float(qty) * value * ybdbrate
                                                userinfo3.save()
                                                # BDB FEE ++
                                                bdbfee.total = bdbfee.total + float(qty) * value * ybdbrate
                                                bdbfee.save()
                                                # ADA全拿
                                                userinfo2.total = userinfo2.total + float(qty)
                                                userinfo2.canuse = userinfo2.canuse + float(qty)
                                                userinfo2.save()
                                                # 生成單
                                                create(time.time(), account, "buy", i.price, qty,
                                                       float(i.price) * float(qty), 2, "market", symbol, memuuid,
                                                       time.time(), relatedid, 0, float(qty) * ybdbrate * value)
                                            # BDB不夠抵
                                            else:

                                                # 先算出VAL = ADA轉成BDB先扣看還有多少
                                                val = float(qty) * value * ybdbrate - userinfo3.canuse
                                                # val2 = 扣完轉回ADA看要扣多少
                                                val2 = val / value / ybdbrate
                                                # 扣掉的到BDBFEE++
                                                bdbfee.total = bdbfee.total + userinfo3.canuse
                                                bdbfee.save()
                                                # 手上的BDB歸零
                                                userinfo3.total = userinfo3.total - userinfo3.canuse
                                                userinfo3.canuse = 0
                                                userinfo3.save()
                                                # user ADA 拿到比原本少一點
                                                userinfo2.total = userinfo2.total + float(qty) - val2 * nbdbrate
                                                userinfo2.canuse = userinfo2.canuse + float(qty) - val2 * nbdbrate
                                                userinfo2.save()
                                                # ADA fee ++
                                                leftfee.total = leftfee.total + val2 * nbdbrate
                                                leftfee.save()

                                        qty = 0
                                        if qty == 0:
                                            break
                                if qty == 0:
                                    Res = Response("ok", "success")
                                    return Res
                            else:
                                Res = Response(-1010, "Account has insufficient balance for requested action")
                                return Res
                        else:
                            Res = Response(-904, "Order is not enough")
                            return Res
                    except Exception as e:
                        tp, val, tb = sys.exc_info()
                        error = str(val), str(tb)
                        print(str(error), str(tb.tb_lineno))
                        Res = Response(-904, "Order is not enough")
                        return Res
                except Exception as e:
                    tp, val, tb = sys.exc_info()
                    error = str(val), str(tb)
                    print(str(error), str(tb.tb_lineno))
                    Res = Response(-904, "Order is not enough")
                    return Res
            else:
                Res = Response(-1010, "Account has insufficient balance for requested action")
                return Res
    except Exception as e:
        tp, val, tb = sys.exc_info()
        error = str(val), str(tb)
        print(str(error), str(tb.tb_lineno))
        Res = Response(-1000, "UNKNOWN")
        return Res


def marketsell(account, qty, symbol, memuuid, ybdbrate, nbdbrate, bdbuse, value,
               value2):  ################################::::::::

    try:

        qtytmp = qty
        # ex symbol = ADA_USDT
        coin2 = symbol.split("_")[1]  # coin2 = USDT
        coin3 = symbol.split("_")[0]  # coin3 = ADA
        userinfo = balance.objects.get(coin_id=coin2, memuuid_id=memuuid)  # USDT
        userinfo2 = balance.objects.get(coin_id=coin3, memuuid_id=memuuid)  # ADA
        userinfo3 = balance.objects.get(coin_id="BDB", memuuid_id=memuuid)  # how many BDB

        leftfee = fee.objects.get(coin=coin3)
        rightfee = fee.objects.get(coin=coin2)
        bdbfee = fee.objects.get(coin="BDB")

        with transaction.atomic():

            money = userinfo.canuse  # 先知道有多少ADA可以扣
            # 這邊概念是: 先看錢夠不夠，然後看現在買完該數量(price * qty)要多少錢(cost)，看可用資產(money)夠不夠扣cost，若可扣(>=)再作後續交易。
            if (float(money) > 0):
                try:  # 先看單上總量夠不夠
                    count = order.objects.filter(side='buy', status=0, symbol_id=symbol).filter(
                        ~Q(memuuid_id=memuuid)).aggregate(Sum('qty'))
                    count = count['qty__sum']
                    selllist = order.objects.filter(side="buy", symbol_id=symbol, status=0).filter(
                        ~Q(memuuid_id=memuuid)).order_by("-price", "-timestamp")  # 找出未完成交易的賣單
                    cost = 0  # 這個要記買完全部要多少錢
                    #####這邊只是為了知道買不買得起想要的總數#####
                    try:  # 這個try以防沒有單子

                        if float(count) >= float(qty):
                            relatedid = relatedID()
                            for i in selllist:  # 從最貴的單子開始算數量
                                if float(qty) >= float(i.qty):  # ex: 想買1000，單上700 => 還要賣300 & cost 累加 300
                                    qty = float(qty) - float(i.qty)
                                    cost = cost + float(i.qty)
                                else:  # 要的數量 < 單上 ex: 想要賣200 單上1000 & 10$ => 只累加cost 200(qty)
                                    cost = cost + float(qty)
                                    qty = 0
                            if (cost) <= float(money):  # 真的數量夠賣的話再交易, 這的cost是數量

                                def create(timestamp, account, side, price, qty, total, status, cate, symbol_id,
                                           memuuid_id, tradetime, relatedid, commission, commission2):
                                    unit = order.objects.create(timestamp=timestamp, account=account, side=side,
                                                                price=price, qty=qty, total=total, status=status,
                                                                cate="market",
                                                                symbol_id=symbol_id, memuuid_id=memuuid_id,
                                                                tradetime=tradetime, relatedid=relatedid,
                                                                commission=commission, commission2=commission2)
                                    unit.save()

                                qty = qtytmp
                                for i in selllist:  # 從最貴的單子開始算數量
                                    buyerinfo = balance.objects.get(coin_id=coin2,
                                                                    memuuid_id=i.memuuid_id)  # 處理掛單user的USDT (-)
                                    buyerinfo2 = balance.objects.get(coin_id=coin3,
                                                                     memuuid_id=i.memuuid_id)  # 處理掛單user的ADA (+)
                                    buyerinfo3 = balance.objects.get(coin_id="BDB",
                                                                     memuuid_id=i.memuuid_id)  # 掛單USER的BDB
                                    buyerbdb = check_usebdb(i.account)
                                    if float(qty) >= float(
                                            i.qty):  # ex: 想賣1000，單上700 & $10 => 還要賣300，此時要生成一個交易成功700(i.qty) + 原本單上的要改成成功

                                        qty = float(qty) - float(i.qty)  # 還要買300

                                        i.status = 1
                                        i.tradetime = time.time()
                                        i.relatedid = relatedid
                                        i.save()  # 單上的要改成成功

                                        buyerinfo.total = buyerinfo.total - float(i.price) * float(
                                            i.qty)  # 單上的人會-USDT(單上價格(i.price) * 單數量(i.qty))
                                        buyerinfo.freeze = buyerinfo.freeze - float(i.price) * float(
                                            i.qty)  # 單上的人會-USDT(單上價格(i.price) * 單數量(i.qty))

                                        buyerinfo.save()
                                        userinfo2.total = userinfo.total - float(i.qty)  # 發起交易的減少ADA
                                        userinfo2.canuse = userinfo.canuse - float(i.qty)  # 發起交易的減少ADA
                                        userinfo2.save()

                                        if not buyerbdb:  # 如果單上的人沒有開BDB
                                            buyerdiscount = i.memuuid.level.discount

                                            # 單上的ADATOTAL會增加
                                            buyerinfo2.total = buyerinfo2.total + (float(i.qty)) * (1 - buyerdiscount)
                                            # 單上的ADACANUSE會增加
                                            buyerinfo2.canuse = buyerinfo2.canuse + (float(i.qty)) * (1 - buyerdiscount)
                                            buyerinfo2.save()
                                            leftfee.total = leftfee.total + (float(i.qty)) * buyerdiscount
                                            leftfee.save()
                                            # 單上手續費更新
                                            i.commission2 = (float(i.qty)) * buyerdiscount
                                            i.save()


                                        else:  # 如果單上的人有開

                                            buyerdiscount2 = i.memuuid.level.bdbdiscount  # 2 = 有開的折扣
                                            buyerdiscount = i.memuuid.level.discount  # = 正常折扣
                                            if buyerinfo3.canuse >= (
                                            float(i.qty)) * buyerdiscount2 * value2:  # 如果BDB夠抵銷

                                                # 單上ADA領全部
                                                buyerinfo2.total = buyerinfo2.total + (
                                                    float(i.qty))  # 單上的人會+ADA 單數量(i.qty)
                                                buyerinfo2.canuse = buyerinfo2.canuse + (
                                                    float(i.qty))  # 單上的人會+ADA 單數量(i.qty)
                                                buyerinfo2.save()
                                                # 只扣BDB
                                                buyerinfo3.total = buyerinfo3.total - (
                                                    float(i.qty)) * buyerdiscount2 * value2
                                                buyerinfo3.canuse = buyerinfo3.canuse - (
                                                    float(i.qty)) * buyerdiscount2 * value2
                                                buyerinfo3.save()
                                                # BDB收
                                                bdbfee.total = bdbfee.total + (float(i.qty)) * buyerdiscount2 * value2
                                                bdbfee.save()
                                                i.commission = (float(i.qty)) * buyerdiscount2 * value2
                                                i.save()


                                            else:  # 1000 * 0.001 * 10 - BDB

                                                # 不夠付的BDB 須扣 - 僅剩
                                                val = (float(i.qty)) * buyerdiscount2 * value2 - buyerinfo3.canuse
                                                # 轉成原本不夠付的coin, 還需要付的ADA手續費
                                                val2 = val / buyerdiscount2 / value2
                                                # 收BDB
                                                bdbfee.total = bdbfee.total + buyerinfo3.canuse
                                                bdbfee.save()
                                                # 手續費
                                                i.commission = buyerinfo3.canuse
                                                i.save()
                                                # 把BDB扣完
                                                buyerinfo3.total = buyerinfo3.total - buyerinfo3.canuse
                                                buyerinfo3.canuse = 0

                                                buyerinfo3.save()
                                                # 單上領ADA - 剩下的手續費
                                                buyerinfo2.total = buyerinfo2.total + i.qty - (val2 * buyerdiscount)
                                                buyerinfo2.canuse = buyerinfo2.canuse + i.qty - (val2 * buyerdiscount)
                                                buyerinfo2.save()
                                                # ADAFEE 增加手續費
                                                leftfee.total = leftfee.total + val2 * buyerdiscount
                                                leftfee.save()
                                                # 非BDB手續費
                                                i.commission2 = val2 * (buyerdiscount)
                                                i.save()

                                        if ybdbrate == 0:  # no bdb

                                            # USER領比較少的USDT
                                            userinfo.total = userinfo.total + float(i.qty) * float(i.price) * (
                                                        1 - nbdbrate)  # 發起交易的+USDT
                                            userinfo.canuse = userinfo.canuse + float(i.qty) * float(i.price) * (
                                                        1 - nbdbrate)  # 發起交易的+USDT
                                            userinfo.save()
                                            # USDTFEE拿少拿的
                                            rightfee.total = rightfee.total + float(i.qty) * float(i.price) * (nbdbrate)
                                            rightfee.save()


                                        else:  # use bdb  # * value -> coin to bdb

                                            # 如果使用者BDB夠支付
                                            if userinfo3.canuse >= float(i.qty) * float(i.price) * ybdbrate * value:

                                                # 把BDB扣了(USDT)
                                                userinfo3.total = userinfo3.total - (
                                                            float(i.qty) * float(i.price) * ybdbrate * value)
                                                userinfo3.canuse = userinfo3.canuse - (
                                                            float(i.qty) * float(i.price) * ybdbrate * value)
                                                userinfo3.save()
                                                # 扣的BDB跑到這
                                                bdbfee.total = bdbfee.total + float(i.qty) * float(
                                                    i.price) * ybdbrate * value
                                                bdbfee.save()
                                                # USDT全拿
                                                userinfo.total = userinfo.total + float(i.qty) * float(i.price)
                                                userinfo.canuse = userinfo.canuse + float(i.qty) * float(i.price)
                                                userinfo.save()

                                            else:  # bdb not enough

                                                # val = 還沒付掉的手續費 bdb ADA
                                                val = float(i.qty) * ybdbrate * value2 - userinfo3.canuse
                                                # 以支付的部分先收
                                                bdbfee.total = bdbfee.total + userinfo3.canuse
                                                bdbfee.save()
                                                # 把bdb歸零 當作付光了
                                                userinfo3.total = userinfo3.total - userinfo3.canuse
                                                userinfo3.canuse = 0
                                                userinfo3.save()
                                                # val2 = 還沒付掉的手續號usdt
                                                val2 = (val / value2) / ybdbrate  # 剩下要扣在獲得的coin的 所以乘nbd
                                                # usdt加上原本的再減掉手續費
                                                userinfo.canuse = userinfo.canuse + float(i.qty) * float(
                                                    i.price) - val2 * nbdbrate
                                                userinfo.total = userinfo.total + float(i.qty) * float(
                                                    i.price) - val2 * nbdbrate
                                                userinfo.save()
                                                # 付的usdt手續
                                                rightfee.total = rightfee.total + val2 * nbdbrate
                                                rightfee.save()

                                        if qty == 0:
                                            break
                                    else:  # ex: 賣1000(qty)，單上1300(i.qty) => 想賣0(qty) 單上300(i.qty-qty)，生成兩個成功的1000(qty)

                                        i.qty = float(i.qty) - float(qty)
                                        i.total = float(i.price) * float(i.qty)
                                        i.save()  # 單上300(i.qty-qty)

                                        # unit2 = order.objects.create(timestamp = time.time(), account = i.account, side = "buy", price = i.price, qty = qty, total = float(i.price)*float(qty), status = 1, cate = i.cate,
                                        # 							symbol_id = symbol, memuuid_id = i.memuuid_id, tradetime = time.time(), relatedid = relatedid)
                                        # unit2.save() #生成兩個成功的1000(qty)

                                        userinfo2.total = userinfo2.total - float(qty)  # 發起交易的-ADA(想買數量(qty))
                                        userinfo2.canuse = userinfo2.canuse - float(qty)  # 發起交易的-ADA(想買數量(qty))
                                        userinfo2.save()
                                        buyerinfo.total = buyerinfo.total - float(qty) * float(
                                            i.price)  # 單上的人會-USDT 數量(qty)
                                        buyerinfo.freeze = buyerinfo.freeze - float(qty) * float(
                                            i.price)  # 單上的人會-USDT 數量(qty)
                                        float(qty) * float(i.price)
                                        buyerinfo.save()

                                        if not buyerbdb:  # 如果單上的人沒有開BDB

                                            buyerdiscount = i.memuuid.level.discount
                                            # 拿到實際的(1- n%)
                                            buyerinfo2.total = buyerinfo2.total + (float(qty)) * (
                                                        1 - buyerdiscount)  # 單上的人會+ADA 數量(qty)
                                            buyerinfo2.canuse = buyerinfo2.canuse + (float(qty)) * (
                                                        1 - buyerdiscount)  # 單上的人會+ADA 數量(qty)
                                            buyerinfo2.save()
                                            # 把n%收起來
                                            leftfee.total = leftfee.total + (float(qty)) * buyerdiscount
                                            leftfee.save()
                                            # 生成一筆自己的單
                                            create(time.time(), i.account, "buy", i.price, qty,
                                                   float(i.price) * float(qty), 1, "limit", symbol, i.memuuid_id,
                                                   time.time(), relatedid, 0, (float(qty)) * buyerdiscount)



                                        else:

                                            buyerdiscount2 = i.memuuid.level.bdbdiscount
                                            buyerdiscount = i.memuuid.level.discount
                                            if buyerinfo3.canuse >= (float(qty)) * buyerdiscount2 * value2:  # 如果BDB夠抵銷

                                                # 實際拿多少就多少 只扣bdb
                                                buyerinfo2.total = buyerinfo2.total + (float(qty))  # 單上的人會+ADA 單數量(qty)
                                                buyerinfo2.canuse = buyerinfo2.canuse + (
                                                    float(qty))  # 單上的人會+ADA 單數量(qty)
                                                buyerinfo2.save()
                                                # bdb會拿(1-n%)
                                                buyerinfo3.total = buyerinfo3.total - (
                                                    float(qty)) * buyerdiscount2 * value2
                                                buyerinfo3.canuse = buyerinfo3.canuse - (
                                                    float(qty)) * buyerdiscount2 * value2
                                                buyerinfo3.save()
                                                # (bdb)拿 %
                                                bdbfee.total = bdbfee.total + (float(qty)) * buyerdiscount2 * value2
                                                bdbfee.save()
                                                # 生成一筆自己的單
                                                create(time.time(), i.account, "buy", i.price, qty,
                                                       float(i.price) * float(qty), 1, "limit", symbol, i.memuuid_id,
                                                       time.time(), relatedid, (float(qty)) * buyerdiscount2 * value2,
                                                       0)

                                            else:

                                                # 數量 * bdb折扣 * 價值2 - buyerinfo3.canuse
                                                val = (float(
                                                    qty)) * buyerdiscount2 * value2 - buyerinfo3.canuse  # 不夠付的BDB
                                                # 數量 / 折扣 / 價值 = 不夠付的ada
                                                val2 = val / buyerdiscount2 / value2  # 轉成原本不夠付的coin
                                                # 把原本不夠的bdb存進去
                                                bdbfee.total = bdbfee.total + buyerinfo3.canuse
                                                bdbfee.save()
                                                # 原本的bdb歸零
                                                buyerinfo3.total = buyerinfo3.total - buyerinfo3.canuse
                                                buyerinfo3.canuse = 0
                                                buyerinfo3.save()
                                                # 拿ada實際拿的再減掉手續費val2 * (buyerdiscount)
                                                buyerinfo2.total = buyerinfo2.total + float(qty) - val2 * (
                                                    buyerdiscount)
                                                buyerinfo2.canuse = buyerinfo2.canuse + float(qty) - val2 * (
                                                    buyerdiscount)
                                                buyerinfo2.save()
                                                # fee存進去val2 * (1 - buyerdiscount)
                                                leftfee.total = leftfee.total + val2 * buyerdiscount
                                                leftfee.save()
                                                # 生成一筆自己的單
                                                create(time.time(), i.account, "buy", i.price, qty,
                                                       float(i.price) * float(qty), 1, "limit", symbol, i.memuuid_id,
                                                       time.time(), relatedid, buyerinfo3.canuse, val2 * buyerdiscount)

                                        if ybdbrate == 0:  # no bdb

                                            # BDB不扣 只扣自己得到的
                                            userinfo.total = userinfo.total + float(qty) * float(i.price) * (
                                                        1 - nbdbrate)  # 發起交易的+USDT
                                            userinfo.canuse = userinfo.canuse + float(qty) * float(i.price) * (
                                                        1 - nbdbrate)  # 發起交易的+USDT
                                            userinfo.save()
                                            rightfee.total = rightfee.total + float(qty) * float(i.price) * (nbdbrate)
                                            rightfee.save()

                                            create(time.time(), account, "sell", i.price, qty,
                                                   float(i.price) * float(qty), 2, "market", symbol, memuuid,
                                                   time.time(), relatedid, 0, float(qty) * float(i.price) * (nbdbrate))


                                        else:  # use bdb  # * value -> coin to bdb

                                            # 如果BDB夠支付
                                            if userinfo3.canuse >= float(qty) * float(i.price) * ybdbrate * value:

                                                # 只用BDB支付(USDT) float(qty) * float(i.price) * ybdbrate * value
                                                userinfo3.total = userinfo3.total - float(qty) * float(
                                                    i.price) * ybdbrate * value
                                                userinfo3.canuse = userinfo3.canuse - float(qty) * float(
                                                    i.price) * ybdbrate * value
                                                userinfo3.save()
                                                # BDB存float(qty) * float(i.price) * ybdbrate * value
                                                bdbfee.total = bdbfee.total + float(qty) * float(
                                                    i.price) * ybdbrate * value
                                                bdbfee.save()
                                                # USDT全拿
                                                userinfo.total = userinfo.total + float(qty) * float(
                                                    i.price)  # 發起交易的+USDT
                                                userinfo.canuse = userinfo.canuse + float(qty) * float(
                                                    i.price)  # 發起交易的+USDT
                                                userinfo.save()
                                                # 生成一筆自己的單

                                                create(time.time(), account, "sell", i.price, qty,
                                                       float(i.price) * float(qty), 2, "market", symbol, memuuid,
                                                       time.time(), relatedid, 0,
                                                       float(qty) * float(i.price) * ybdbrate * value)

                                            else:  # bdb not enough

                                                # BDB付不夠 先知道缺多少BDB (這單多少 - 剩多少 = 還差多少)
                                                val = float(qty) * float(
                                                    i.price) * ybdbrate * value - userinfo3.canuse  # val = 花不起的BDB ####
                                                # 把剩的存起來當作已付
                                                bdbfee.total = bdbfee.total + userinfo3.canuse
                                                bdbfee.save()
                                                # BDB除VALUE除率	 = USDT
                                                val2 = (val / value) / ybdbrate  # 剩下要扣在獲得的coin的 所以乘nbd

                                                create(time.time(), account, "sell", i.price, qty,
                                                       float(i.price) * float(qty), 2, "market", symbol, memuuid,
                                                       time.time(), relatedid, userinfo3.canuse, val2 * nbdbrate)
                                                # 花光所以歸零
                                                userinfo3.total = userinfo3.total - userinfo3.canuse
                                                userinfo3.canuse = 0
                                                userinfo3.save()
                                                # USER USDT加上全部 - 手續費
                                                userinfo.canuse = userinfo.canuse + float(qty) * float(
                                                    i.price) - val2 * nbdbrate
                                                userinfo.total = userinfo.total + float(qty) * float(
                                                    i.price) - val2 * nbdbrate
                                                userinfo.save()
                                                # 手續費
                                                rightfee.total = rightfee.total + val2 * nbdbrate
                                                rightfee.save()

                                        qty = 0
                                        if qty == 0:
                                            break
                                if qty == 0:
                                    Res = Response("ok", "success")
                                    return Res
                            else:
                                Res = Response(-1010, "Account has insufficient balance for requested action")
                                return Res
                        else:
                            Res = Response(-904, "Order is not enough")
                            return Res

                    except Exception as e:
                        tp, val, tb = sys.exc_info()
                        error = str(val), str(tb)
                        print(str(error), str(tb.tb_lineno))
                        Res = Response(-904, "Order is not enough")
                        return Res
                except Exception as e:
                    tp, val, tb = sys.exc_info()
                    error = str(val), str(tb)
                    print(str(error), str(tb.tb_lineno))
                    Res = Response(-904, "Order is not enough")
                    return Res
            else:
                Res = Response(-1010, "Account has insufficient balance for requested action")
                return Res
    except Exception as e:
        tp, val, tb = sys.exc_info()
        error = str(val), str(tb)
        print(str(error), str(tb.tb_lineno))
        Res = Response(-1000, "UNKNOWN")
        return Res


def relatedID():
    maxID = order.objects.all().aggregate(Max('relatedid'))
    relatedid = maxID['relatedid__max']
    try:
        return (maxID['relatedid__max'] + 1)
    except Exception as e:
        tp, val, tb = sys.exc_info()
        error = str(val), str(tb)
        print(str(error), str(tb.tb_lineno))
        return 1


def check_cancel(tmp):
    canceltime = 300  # N秒內取消記數

    try:
        if (time.time()) - float(tmp.cancel_time) <= canceltime:
            tmp.cancel_num = int(tmp.cancel_num) + 1
            tmp.save()
        else:
            tmp.cancel_num = 1
            tmp.cancel_time = time.time()
            tmp.save()

    except Exception as e:
        tp, val, tb = sys.exc_info()
        error = str(val), str(tb)
        print(str(error), str(tb.tb_lineno))


def check_userlevel(tmp):
    try:
        if int(tmp.level.level) > 0:
            return True
        return False
    except Exception as e:
        tp, val, tb = sys.exc_info()
        error = str(val), str(tb)
        print(str(error), str(tb.tb_lineno))
