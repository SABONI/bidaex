from django.db import models

# Create your models here.
class deposit(models.Model):  # 充值
    memuuid = models.ForeignKey('accounts.User', on_delete=models.CASCADE)                            #UUID(FK=>User
    timestamp = models.BigIntegerField(db_column='timestamp', null=True)                     #成功時間
    status = models.CharField(db_column='status', max_length=20)                             # 狀態
    coin = models.ForeignKey('accounts.coin',on_delete=models.CASCADE)                                #幣值名(FK=>coin
    qty = models.CharField(db_column='qty', max_length=20)                                   # 顆數
    address = models.CharField(db_column='address', max_length=50, null=True)                # 地址
    txhash = models.CharField(db_column='txhash', max_length=50, null=True)                  # txid
    chain = models.CharField(db_column='chain', max_length=50, null=True)                    # 鏈
    def get_status(self):
        return

class withdrawals(models.Model):  # 提現
    memuuid = models.ForeignKey('accounts.User', on_delete=models.CASCADE)                            # UUID(FK=>User
    timestamp = models.BigIntegerField(db_column='timestamp', null=True)                     # timestamp
    status = models.CharField(db_column='status', max_length=20)                             # 狀態
    coin = models.ForeignKey('accounts.coin', on_delete=models.CASCADE)                               # 幣值名(FK=>coin
    qty = models.CharField(db_column='qty', max_length=20)                                   # 顆數
    address = models.CharField(db_column='address', max_length=50, null=True)                # 地址
    txhash = models.CharField(db_column='txhash', max_length=50, null=True)                  # txid
    chain = models.CharField(db_column='chain', max_length=50, null=True)                    # 鏈
    verifynum = models.CharField(db_column='verifynum', max_length=20)                       # 驗證次數
    def get_status(self):
        return
